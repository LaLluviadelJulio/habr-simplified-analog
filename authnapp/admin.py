from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from social_django.admin import AssociationOption, NonceOption, UserSocialAuthOption
from social_django.models import Association, Nonce, UserSocialAuth

from authnapp.models import HabrUser, HabrUserProfile


class HabrUserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets
    fieldsets[1][1]['fields'] = fieldsets[1][1]['fields'] + (
        'age', 'banned_until', 'avatar',
    )

    add_fieldsets = UserAdmin.add_fieldsets
    add_fieldsets[0][1]['fields'] = add_fieldsets[0][1]['fields'] + (
        'email', 'first_name', 'last_name', 'age',
    )

    list_display = ('avatar_tag', 'username', 'first_name',
                    'last_name', 'email', 'age', 'is_active', 'banned_until', 'likes')
    list_display_links = ('username',)
    search_fields = ('username', 'first_name', 'last_name', 'is_active')
    readonly_fields = ['avatar_tag']


class HabrUserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'gender')
    list_display_links = ('user', 'gender')


admin.site.register(HabrUser, HabrUserAdmin)
admin.site.register(HabrUserProfile, HabrUserProfileAdmin)


class AssociationProxy(Association):
    class Meta:
        proxy = True
        verbose_name_plural = 'Ассоциации'
        verbose_name = 'Ассоциация'
        app_label = 'social_django'


class NonceProxy(Nonce):
    class Meta:
        proxy = True
        verbose_name_plural = 'Одноразовые (временные) профили'
        verbose_name = 'Одноразовый (временный) профиль'
        app_label = 'social_django'


class UserSocialAuthProxy(UserSocialAuth):
    class Meta:
        proxy = True
        verbose_name_plural = 'Профили из соцсетей'
        verbose_name = 'Профиль из соцсети'
        app_label = 'social_django'


admin.site.unregister(Association)
admin.site.unregister(Nonce)
admin.site.unregister(UserSocialAuth)
admin.site.register(AssociationProxy, AssociationOption)
admin.site.register(NonceProxy, NonceOption)
admin.site.register(UserSocialAuthProxy, UserSocialAuthOption)
