from django.urls import path, include
from rest_framework.routers import DefaultRouter

import authnapp.views as authnapp
from authnapp.apps import AuthnappConfig
from authnapp.views import ProfileView, ProfilePublishedArticlesView, \
    ImageListView, PictureFileCreateView, PictureFileDeleteView, \
    PictureFileUpdateView, MyCommentsListView, ToMeCommentsListView, \
    SearchImageView, ProfileDraftsArticlesView, ProfileDeniedArticlesView, \
    ProfileOnModerationArticlesView, MyMessagesListView, \
    ProfileArticleToModerationView, MessageIsReadView
from .api import HabrUserApi

arouter = DefaultRouter()
arouter.register('habrusers', HabrUserApi)

app_name = AuthnappConfig.name

urlpatterns = [
    path("login/", authnapp.login, name="login"),
    path("logout/", authnapp.logout, name="logout"),
    path("register/", authnapp.register, name="register"),
    path("edit/", authnapp.edit, name="edit"),
    path("password_reset/", authnapp.MyPasswordResetView.as_view(), name='password_reset'),
    path("password_reset_done/", authnapp.MyPasswordResetDone.as_view(), name='password_reset_done'),
    path("reset/<uidb64>/<token>/", authnapp.MyPasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path("password_reset_complete/", authnapp.MyPasswordResetCompleteView.as_view(), name='password_reset_complete'),

    path('profile/<uuid:pk>/', ProfileView.as_view(), name='profile'),
    path('published_articles/<uuid:pk>/', ProfilePublishedArticlesView.as_view(), name='published_user_articles'),
    path('draft_articles/<uuid:pk>/', ProfileDraftsArticlesView.as_view(), name='draft_user_articles'),
    path('denied_articles/<uuid:pk>/', ProfileDeniedArticlesView.as_view(), name='denied_user_articles'),
    path('on_moderation_articles/<uuid:pk>/', ProfileOnModerationArticlesView.as_view(), name='on_moderation_user_articles'),
    path('article_to_moderation/<slug:article_slug>/', ProfileArticleToModerationView.as_view(),
         name='article_to_moderation'),

    path('api/', include(arouter.urls)),

    path('images/<uuid:pk>/', ImageListView.as_view(), name='user_images'),
    path('load_images/<uuid:pk>/', PictureFileCreateView.as_view(), name='load_images'),
    path('delete_images/<slug:picture_slug>/', PictureFileDeleteView.as_view(),
         name='delete_images'),
    path('update_images/<slug:picture_slug>/', PictureFileUpdateView.as_view(),
         name='update_images'),
    path('search_images/<uuid:pk>/', SearchImageView.as_view(), name='search_images'),

    path('my_comments/<uuid:pk>/', MyCommentsListView.as_view(), name='user_comments'),
    path('to_me_comments/<uuid:pk>/', ToMeCommentsListView.as_view(), name='to_me_comments'),

    path('my_messages/<uuid:pk>/', MyMessagesListView.as_view(), name='user_messages'),
    path('read_message/<slug:slug>/', MessageIsReadView.as_view(), name='read_message'),
]
