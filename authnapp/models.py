import logging
from datetime import datetime, timedelta
from logging import Logger
from uuid import uuid4

from PIL import Image, ExifTags
from django.contrib.auth.models import AbstractUser
from django.core.validators import EmailValidator, MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.safestring import mark_safe
from django.utils.timezone import now

logger: Logger = logging.getLogger(__name__)

AVATAR_MAX_SIZE = 300

def user_avatar_path(instance, filename):
    """Generates and returns the path for the saved avatar of the user.
        It is necessary for the orderly storage of images.

        Args:

            * instance (`HabrUser`): an instance of a user being created or modified.
            * filename (`str`): the name under which the image will be stored.

        Returns:

            * str: the path to the saved file.
        """
    return f'media/{instance.username}/user_avatar/{filename}'


class HabrUser(AbstractUser):
    """The model for the user."""
    uuid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    username = models.CharField(verbose_name="логин", max_length=128, blank=False, unique=True)
    first_name = models.CharField(verbose_name="имя", max_length=128, blank=False)
    last_name = models.CharField(verbose_name="фамилия", max_length=128, blank=False)
    avatar = models.ImageField(upload_to=user_avatar_path, blank=True)
    email = models.CharField(verbose_name="email", max_length=128,
                             unique=True, blank=False, validators=[EmailValidator])
    age = models.PositiveIntegerField(verbose_name="возраст", default=18, validators=[
        MinValueValidator(18), MaxValueValidator(99)])
    registration_time = models.DateTimeField(verbose_name="время регистрации", default=datetime.now)
    is_active = models.BooleanField(verbose_name="пользователь активен",
                                    default=True, db_index=True)
    is_superuser = models.BooleanField(verbose_name="администратор", default=False)
    is_staff = models.BooleanField(verbose_name="модератор", default=False)
    activation_key = models.CharField(verbose_name="ключ подтверждения", max_length=128, blank=True)
    activation_key_expires = models.DateTimeField(verbose_name="актуальность ключа",
                                                  default=datetime.now() + timedelta(hours=48))
    banned_until = models.DateTimeField(verbose_name="забанен до", blank=True, null=True)
    likes = models.IntegerField(verbose_name="лайки", default=0)


    def __str__(self):
        """Forms a printable representation of the object.
        Returns the username of the user.
        """
        return str(self.username)

    def save(self, *args, **kwargs):
        """Saves a new object, if there is an image file, if necessary,
        reduces it to an acceptable size and expands it for correct display."""
        super().save(*args, **kwargs)
        if self.avatar:
            try:
                width, height = self.avatar.width, self.avatar.height
                max_size = max(width, height)
                img = Image.open(self.avatar.path)
                if max_size > AVATAR_MAX_SIZE:
                    img = img.resize(
                        (round(width / max_size * AVATAR_MAX_SIZE),
                         round(height / max_size * AVATAR_MAX_SIZE)),
                        Image.ANTIALIAS
                    )
                for orientation in ExifTags.TAGS.keys():
                    if ExifTags.TAGS[orientation] == 'Orientation':
                        break
                exif = dict(img.getexif().items())
                image = img.rotate({3: 180, 6: 270, 8: 90}.get(exif[orientation], 0), expand=True)
                image.save(self.avatar.path)
            except (AttributeError, KeyError, IndexError) as error:
                logging.warning(error, exc_info=True)

    def delete(self, using=None, keep_parents=False):
        """When an object is deleted, it deletes the corresponding image file."""
        if self.avatar:
            self.avatar.delete(save=False)
        super().delete()

    def get_likes(self):
        """Counting the likes received by the user."""
        from articles.models import Like
        likes = Like.objects.all().filter(like_user=self).count()
        return likes

    def get_articles(self):
        """Counting the likes received by the user."""
        from articles.models import Article
        articles = Article.objects.all().filter(user_id=self).count()
        return articles

    @property
    def get_avatar(self):
        """Returns the path to the avatar file."""
        if self.avatar:
            return self.avatar.url
        return '/static/vendor/img/default.png'

    def avatar_tag(self):
        """Returns a html element that has the path to the avatar file embedded in it
        in order to display it correctly in the admin panel."""
        return mark_safe(f'<img src="{self.get_avatar}" width="100%" height="100%" />')

    avatar_tag.short_description = 'Аватар'

    class Meta:
        """Ordering users according to their id.
        """
        ordering = ('uuid',)
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def is_activation_key_expired(self):
        """Checks whether the profile activation period has expired."""
        if now() <= self.activation_key_expires:
            return False
        return True


class HabrUserProfile(models.Model):
    MALE = "M"
    FEMALE = "F"

    GENDER_CHOICES = (
        (MALE, "М"),
        (FEMALE, "Ж"),
    )

    uuid = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    user = models.OneToOneField(
        HabrUser, unique=True, null=False, db_index=True, on_delete=models.CASCADE
    )
    tagline = models.CharField(verbose_name="теги", max_length=128, blank=True)
    aboutMe = models.TextField(verbose_name="о себе", max_length=512, blank=True)
    gender = models.CharField(
        verbose_name="пол", max_length=1, choices=GENDER_CHOICES, blank=True
    )

    @receiver(post_save, sender=HabrUser)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            HabrUserProfile.objects.create(user=instance)

    @receiver(post_save, sender=HabrUser)
    def save_user_profile(sender, instance, **kwargs):
        instance.habruserprofile.save()

    class Meta:
        verbose_name = "Профиль пользователя"
        verbose_name_plural = "Профили пользователей"
