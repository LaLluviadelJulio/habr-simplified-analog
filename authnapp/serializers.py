from rest_framework import serializers
from .models import HabrUser


class HabrUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = HabrUser
        fields = ['uuid', 'username', 'is_active']
