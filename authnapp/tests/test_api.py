from rest_framework.test import RequestsClient
from django.test import TestCase
from rest_framework.test import APIClient
from requests.auth import HTTPBasicAuth
from django.core.files.uploadedfile import SimpleUploadedFile
from authnapp.models import HabrUser
from articles.models import Tag, Category, Article, Comment, Like, PictureFile, Message, MessageType

username = 'admin'
password = 'dfv68CC$'


class HabrUserTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.admin = HabrUser.objects.create(
            username=username,
            first_name=username,
            is_superuser=True,
            is_staff=True,
            last_name=username,
            email=f'{username}@mail.ru'
        )
        cls.admin.set_password(password)
        cls.admin.save()

        cls.user = HabrUser.objects.create(
            username='user',
            first_name='user',
            last_name='user',
            is_active=True,
            email='user@mail.ru'
        )

    def test_habruser_get(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        response = client.get(f'http://127.0.0.1:8000/auth/api/habrusers/{self.user.uuid}/')
        assert response.status_code == 200

    def test_habruser_update(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {"is_active": False}
        response = client.put(f'http://127.0.0.1:8000/auth/api/habrusers/{self.user.uuid}/', data)
        assert response.status_code == 201
