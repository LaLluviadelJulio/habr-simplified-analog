from django.test import TestCase
from authnapp.models import HabrUser
from django.core.exceptions import ValidationError


class HabrUserModelTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = HabrUser.objects.create(
            username="testuser",
            first_name="testuser",
            last_name="testuser",
            email="testuser@ya.ru"
        )

    def test_it_type(self):
        self.assertIsInstance(self.user, HabrUser)


    def test_email_validator(self):
        with self.assertRaises(ValidationError):
            HabrUser.email.field.run_validators(value="user")
        self.assertIsNone(HabrUser.email.field.run_validators(value="user@yandex.ru"))

    def test_age_validator(self):
        with self.assertRaises(ValidationError):
            HabrUser.age.field.run_validators(value=20)
            HabrUser.age.field.run_validators(value=100)
        self.assertIsNone(HabrUser.age.field.run_validators(value=30))
