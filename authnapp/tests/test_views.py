"""
Contains unit and integration tests for checking the views of the web application.
"""

import logging
import sys

from django.db.models import Q
from django.urls import reverse

from articles.models import Article
from articles.tests.test_views import TestArticleBase
from authnapp.models import HabrUser

if len(sys.argv) > 1 and sys.argv[1] == 'test':
    logging.disable(logging.CRITICAL)


class TestProfileView(TestArticleBase):
    """Testing ProfileView, the view to display the user profile."""

    def test_profile_authorized_only(self):
        """Testing the unavailability of the profile page without user authorization."""
        response = self.client.get(f'/auth/profile/{self.test_user_01.uuid}/')
        self.assertEqual(response.status_code, 302)

    def test_is_response_status_code_correct(self):
        """Checks that the server response code is 200."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(f'/auth/profile/{self.test_user_01.uuid}/')
        self.assertEqual(response.status_code, 200)

    def test_is_template_correct(self):
        """Checks that the view uses the correct template."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(f'/auth/profile/{self.test_user_01.uuid}/')
        self.assertTemplateUsed(response, 'authnapp/includes/profile_data.html')

    def test_is_view_url_accessible_by_name(self):
        """Checks if the view URL is available by name."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(reverse('auth:profile', args=[self.test_user_01.uuid]))
        self.assertEqual(response.status_code, 200)

    def test_does_view_use_correct_title(self):
        """Checks that the view uses the correct title."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(reverse('auth:profile', args=[self.test_user_01.uuid]))
        self.assertEqual(response.context['title'],
                         f'Профиль пользователя {self.test_user_01.username}')


class ProfileArticlesView(TestArticleBase):
    """Testing ProfileArticlesView, the view to display the user's articles."""

    def test_profile_authorized_only(self):
        """Testing the unavailability of the profile page without user authorization."""
        response = self.client.get(f'/auth/articles/{self.test_user_01.uuid}/')
        self.assertEqual(response.status_code, 302)

    def test_is_response_status_code_correct(self):
        """Checks that the server response code is 200."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(f'/auth/articles/{self.test_user_01.uuid}/')
        self.assertEqual(response.status_code, 200)

    def test_is_template_correct(self):
        """Checks that the view uses the correct template."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(f'/auth/articles/{self.test_user_01.uuid}/')
        self.assertTemplateUsed(response, 'authnapp/includes/articles_data.html')

    def test_is_view_url_accessible_by_name(self):
        """Checks if the view URL is available by name."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(reverse('auth:user_articles', args=[self.test_user_01.uuid]))
        self.assertEqual(response.status_code, 200)

    def test_does_view_use_correct_title(self):
        """Checks that the view uses the correct title."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(reverse('auth:user_articles', args=[self.test_user_01.uuid]))
        self.assertEqual(response.context['title'],
                         f'Статьи пользователя {self.test_user_01.username}')

    def test_is_view_uses_context_object_name(self):
        """Checks that the specified context object name is used."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(reverse('auth:user_articles', args=[self.test_user_01.uuid]))
        self.assertTrue('user_articles' in response.context)

    def test_is_paginated(self):
        """Checks that pagination is being performed."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(f'/auth/articles/{self.test_user_01.uuid}/')
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue(len(response.context['user_articles']) == 3)
        #
        response = self.client.get(f'/auth/articles/{self.test_user_01.uuid}/'
                                   + '?page=3')
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue(len(response.context['user_articles']) == 1)

    def test_do_articles_of_current_user_fall_into_queryset(self):
        """Checks that articles of only current user fall into the queryset."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(f'/auth/articles/{self.test_user_01.uuid}/')
        view_articles_author = {article.user for article in response.context['user_articles']}
        current_user = HabrUser.objects.get(uuid=self.test_user_01.uuid)
        self.assertEqual(*view_articles_author, current_user)

    def test_are_articles_ranked_by_date_of_addition(self):
        """Checks that the articles in the page output are ranked by the date of addition."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        user_articles = []
        for page in range(1, 3):
            response = self.client.get(f'/auth/articles/{self.test_user_01.uuid}/'
                                       + f'?page={page}')
            user_articles.extend(response.context['user_articles'])

        for index in range(1, len(user_articles)):
            article_01_date, article_02_date = user_articles[index - 1].registration_time, \
                                               user_articles[index].registration_time
            self.assertTrue(article_01_date > article_02_date)

    def test_does_queryset_contain_only_active_articles(self):
        """Checks that only articles with the 'is_active' field = True gets into the queryset."""
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(f'/auth/articles/{self.test_user_01.uuid}/')
        articles = Article.objects.filter(Q(is_active=True), Q(user=self.test_user_01))
        for articles_set in zip(response.context['user_articles'], articles):
            self.assertEqual(*articles_set)

    def test_no_inactive_articles_in_queryset(self):
        """Checks that inactive articles do not fall into the queryset."""
        article = Article.objects.get(id=1)
        self.client.login(username=self.test_user_01.username, password='QweRtyN')
        response = self.client.get(f'/auth/articles/{self.test_user_01.uuid}/')
        self.assertIn(article, response.context['user_articles'])

        article.is_active = False
        article.save()
        response = self.client.get(f'/auth/articles/{self.test_user_01.uuid}/')
        self.assertNotIn(article, response.context['user_articles'])
