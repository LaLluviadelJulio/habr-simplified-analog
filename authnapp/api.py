from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from django.shortcuts import get_object_or_404
from .serializers import HabrUserSerializer
from .models import HabrUser


class HabrUserApi(ModelViewSet):
    queryset = HabrUser.objects.all()
    serializer_class = HabrUserSerializer
    http_method_names = ['get', 'put']
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, pk=None):
        item = get_object_or_404(self.queryset, pk=pk)
        serializer = HabrUserSerializer(item)
        return Response(serializer.data)

    def update(self, request, pk=None):
        instance = get_object_or_404(self.queryset, pk=pk)
        data = {
            "is_active": request.POST.get('is_active', None),
        }
        serializer = self.serializer_class(instance=instance,
                                           data=data,
                                           context={'is_active': instance.is_active},
                                           partial=True)
        if serializer.is_valid():
            if request.user.is_staff and data['is_active'] != instance.is_active:
                serializer.save()
            else:
                return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
