import logging

from django.conf import settings
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import PasswordResetView, PasswordResetConfirmView, PasswordResetDoneView, \
    PasswordResetCompleteView, reverse_lazy
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.db import transaction
from django.db.models import Q, Count
from django.shortcuts import HttpResponseRedirect, render, redirect
from django.urls import reverse
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.timezone import now
from django.views import View
from django.views.generic import ListView, DetailView, TemplateView, DeleteView, UpdateView

from articles.models import Article, PictureFile, Comment, Message
from articles.views import ArticleLikeRatingView
from authnapp.forms import (HabrUserEditForm, HabrUserLoginForm,
                            HabrUserProfileEditForm, HabrUserRegisterForm, ImageFormSet, ImageUpdatePicturesForm)
from authnapp.models import HabrUser, HabrUserProfile
from habr.mixin import AuthorizedOnlyDispatchMixin
from habr.settings import DOMAIN_NAME

logger = logging.getLogger(__name__)


def login(request):
    title = "Вход в систему"

    login_form = HabrUserLoginForm(data=request.POST or None)

    if request.method == "POST" and login_form.is_valid():
        username = request.POST["username"]
        password = request.POST["password"]
        key = 'next'

        try:
            next_page = request.GET[key]
        except MultiValueDictKeyError:
            logger.error('There is no key %s in the request.GET dictionary that should point to the page '
                         'to which the user will return after authentication. '
                         'The user %s was redirected to the main page.'
                         , key, request.user.username)
            next_page = '/'

        user = auth.authenticate(username=username, password=password)
        if user and user.banned_until and user.banned_until < now():
            user.banned_until = None
            user.save()
        if user and user.is_active:
            auth.login(request, user)
            return HttpResponseRedirect(next_page)

    context = {"title": title, "login_form": login_form}
    return render(request, "authnapp/authentification/login.html", context)


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse("index"))


def register(request):
    title = "Регистрация"

    if request.method == "POST":
        register_form = HabrUserRegisterForm(request.POST, request.FILES)

        if register_form.is_valid():
            register_form.save()
            from articles.models import Message, MessageType
            user = HabrUser.objects.get(username=register_form.cleaned_data['username'])
            try:
                message_sys_type = MessageType.objects.get(sys_type='HELLO')

                Message.objects.create(message_type=message_sys_type,
                                       user=user,
                                       text=f'Добро пожаловать на {DOMAIN_NAME}, дорогой наш '
                                            f'{user.first_name} {user.last_name}!')
            except ObjectDoesNotExist:
                pass
            return HttpResponseRedirect(reverse("auth:login"))
    else:
        register_form = HabrUserRegisterForm()

    context = {"title": title, "register_form": register_form}
    return render(request, "authnapp/authentification/register.html", context)


@login_required
@transaction.atomic
def edit(request):
    title = "Редактирование"

    if request.method == "POST":
        edit_form = HabrUserEditForm(request.POST, request.FILES, instance=request.user)
        profile_form = HabrUserProfileEditForm(request.POST, instance=request.user.habruserprofile)
        if edit_form.is_valid() and profile_form.is_valid():
            edit_form.save()
            return HttpResponseRedirect(reverse('auth:profile', args=[request.user.habruserprofile.uuid]))
        return HttpResponseRedirect(reverse("auth:edit"))
    else:
        edit_form = HabrUserEditForm(instance=request.user)
        profile_form = HabrUserProfileEditForm(instance=request.user.habruserprofile)

    context = {"title": title, "edit_form": edit_form, "profile_form": profile_form, "media_url": settings.MEDIA_URL}

    return render(request, "authnapp/profile/edit.html", context)


def send_verify_mail(user):
    verify_link = reverse("auth:verify", args=[user.email, user.activation_key])

    title = f"Подтверждение учетной записи {user.username}"
    message = f"Для подтверждения учетной записи {user.username} " \
              f"на портале {settings.DOMAIN_NAME} перейдите по ссылке: \
    \n{settings.DOMAIN_NAME}{verify_link}"

    print(f"from: {settings.EMAIL_HOST_USER}, to: {user.email}")
    return send_mail(
        title,
        message,
        settings.EMAIL_HOST_USER,
        [user.email],
        fail_silently=False,
    )


def verify(request, email, activation_key):
    try:
        user = HabrUser.objects.get(email=email)
        if user.activation_key == activation_key and not user.is_activation_key_expired():
            logger.info("User %s is activated.", user)
            user.is_active = True
            user.save()
            auth.login(request, user, backend="django.contrib.auth.backends.ModelBackend")

            return render(request, "authnapp/authentification/verification.html")
        logger.error("Error activation user: %s.", user)
        return render(request, "authnapp/authentification/verification.html")

    except Exception as e:
        logger.error("Error activation user : %s.", e.args)

    return HttpResponseRedirect(reverse("index"))


class MyPasswordResetView(PasswordResetView):
    email_template_name = 'authnapp/authentification/password_reset_email.html'

    success_url = reverse_lazy('auth:password_reset_done')

    template_name = 'authnapp/authentification/password_reset.html'


class MyPasswordResetDone(PasswordResetDoneView):
    template_name = 'authnapp/authentification/password_reset_done.html'
    title = 'Запрос на сброс пароля отправлен'


class MyPasswordResetConfirmView(PasswordResetConfirmView):
    template_name = 'authnapp/authentification/password_reset_confirm.html'
    success_url = reverse_lazy('auth:password_reset_complete')


class MyPasswordResetCompleteView(PasswordResetCompleteView):
    template_name = 'authnapp/authentification/password_reset_complete.html'
    title = 'Сброс пароля завершен'


class ProfileView(DetailView, AuthorizedOnlyDispatchMixin):
    """A view for the users to view their profile."""
    model = HabrUserProfile
    template_name = 'authnapp/includes/profile_data.html'

    def get_context_data(self, **kwargs):
        """Adds the page title to the context and returns it."""
        context = super().get_context_data(**kwargs)
        context['user'] = context['habruserprofile'].user
        context['title'] = f'Профиль пользователя {context["user"].username}'
        return context


class ProfileBaseArticleView(ListView, AuthorizedOnlyDispatchMixin):
    """A base view for viewing a list of articles written by the user.

    Parameters:
        * flag(str): the default value is 'DRAFT'. Switching the parameter
                         allows you to return a queryset of articles with different
                         status:
                         'DRAFT' - drafts only, not published;
                         'DENY' - rejected by the moderator, not published;
                         'ACTIVE' - approved by the moderator, published;
    """
    model = HabrUser
    template_name = 'authnapp/includes/articles_data.html'
    context_object_name = 'user_articles'
    paginate_by = 3
    flag = 'DRAFT'

    def get_queryset(self):
        """Returns a queryset of articles of the specified user.
        The type of articles determines the flag.
        """
        current_user = self.request.user
        return Article.objects.filter(Q(user=current_user), Q(is_active=self.flag))

    def get_context_data(self, **kwargs):
        """Adds the current user and the type of articles to the context and returns it."""
        context = super().get_context_data(**kwargs)
        context['habruserprofile'] = HabrUserProfile.objects.get(user=self.request.user)
        context['flag'] = self.flag
        return context


class ProfilePublishedArticlesView(ProfileBaseArticleView):
    """A view for viewing a list of active articles written by the user."""
    flag = 'ACTIVE'

    def get_context_data(self, **kwargs):
        """Adds the page title to the context and returns it."""
        context = super().get_context_data(**kwargs)
        context['title'] = f'Опубликованные статьи {self.request.user.username}'
        return context


class ProfileDraftsArticlesView(ProfileBaseArticleView):
    """A view for viewing a list of draft articles written by the user."""

    def get_context_data(self, **kwargs):
        """Adds the page title to the context and returns it."""
        context = super().get_context_data(**kwargs)
        context['title'] = f'Черновики {self.request.user.username}'
        return context


class ProfileDeniedArticlesView(ProfileBaseArticleView):
    """A view for viewing a list of denied articles written by the user."""
    flag = 'DENY'

    def get_context_data(self, **kwargs):
        """Adds the page title to the context and returns it."""
        context = super().get_context_data(**kwargs)
        context['title'] = f'Отклоненные статьи {self.request.user.username}'
        return context


class ProfileOnModerationArticlesView(ProfileBaseArticleView):
    """A view for viewing a list of articles submitted for moderation written by the user."""
    flag = 'MODER'

    def get_context_data(self, **kwargs):
        """Adds the page title to the context and returns it."""
        context = super().get_context_data(**kwargs)
        context['title'] = f'Статьи {self.request.user.username}, отправленные на модерацию'
        return context


class ProfileArticleToModerationView(DetailView, AuthorizedOnlyDispatchMixin):
    """View to submit an article for moderation from a user profile."""
    model = Article
    template_name = 'authnapp/includes/articles_data.html'
    slug_url_kwarg = 'article_slug'
    title = ''

    def post(self, *args, **kwargs):
        """Receives article data, changes its status to "MODER" and
        returns redirection to the corresponding page (list of drafts
        or rejected articles in the user'э profile)."""
        article = self.get_object()
        initial_status = article.is_active
        article.is_active = 'MODER'
        article.save()
        if initial_status == 'DENY':
            return redirect(reverse_lazy('auth:denied_user_articles',
                                         args=[self.request.user.uuid]))
        return redirect(reverse_lazy('auth:draft_user_articles',
                                     args=[self.request.user.uuid]))


class ImageListView(ListView, AuthorizedOnlyDispatchMixin):
    """A view for viewing a list of images owned by the user."""
    model = PictureFile
    template_name = 'authnapp/includes/image_data.html'
    context_object_name = 'user_pictures'
    paginate_by = 6

    def get_queryset(self):
        """Returns a queryset of user images."""
        current_user = self.request.user
        return PictureFile.objects.filter(Q(user=current_user), Q(is_active=True))

    def get_context_data(self, **kwargs):
        """Adds the page title and user to the context and returns it."""
        context = super().get_context_data(**kwargs)
        context['title'] = f'Картинки пользователя {self.request.user.username}'
        context['habruserprofile'] = HabrUserProfile.objects.get(user=self.request.user)
        return context


class PictureFileCreateView(TemplateView, AuthorizedOnlyDispatchMixin):
    """View for the user to upload new images."""
    template_name = 'authnapp/profile/profile_load_pictures.html'

    def get(self, *args, **kwargs):
        """Defines and returns the format used."""
        formset = ImageFormSet()
        return self.render_to_response({'image_formset': formset,
                                        'title': 'Загрузка картинок'})

    def post(self, *args, **kwargs):
        """Gets the data of the formset forms, if they are valid, adds the current
        user's photo as the owner and creates the corresponding entries in the database.
        If the data is invalid, it returns a formset with an indication of form filling errors."""
        formset = ImageFormSet(self.request.POST, self.request.FILES)
        if formset.is_valid():
            for form in formset:
                picture_file = form.save(commit=False)
                picture_file.user = self.request.user
                picture_file.save()
            return redirect(reverse_lazy('auth:user_images',
                                         args=[self.request.user.uuid]))

        return render(self.request, self.template_name,
                      context={'image_formset': formset, 'title': 'Загрузка картинок'})


class PictureFileDeleteView(DeleteView, AuthorizedOnlyDispatchMixin):
    """View to deactivate a specific image in the User's Account page."""
    model = PictureFile
    template_name = 'authnapp/profile/profile-update-pictures.html'
    slug_url_kwarg = 'picture_slug'

    def get(self, request, *args, **kwargs):
        """Makes the photo deleted by the user inactive.
        It is no longer displayed in his gallery."""
        item = self.get_object()
        item.is_active = not bool(item.is_active) if item.is_active is True else True
        item.save()
        return redirect(reverse_lazy('auth:user_images',
                                     args=[request.user.uuid]))


class PictureFileUpdateView(UpdateView, AuthorizedOnlyDispatchMixin):
    """View to update a specific image in the User's Account page."""
    model = PictureFile
    template_name = 'authnapp/profile/profile-update-pictures.html'
    form_class = ImageUpdatePicturesForm
    slug_url_kwarg = 'picture_slug'
    success_url = ''

    def get_success_url(self):
        """Returns the url leading to the gallery page in the profile of a particular user."""
        return reverse_lazy('auth:user_images',
                            args=[self.object.user.uuid])

    def get_context_data(self, **kwargs):
        """Returns the context with the addition of the page title."""
        context = super().get_context_data(**kwargs)
        context['title'] = f'Изменение картинки {self.object.file_name}'
        return context


class MyCommentsListView(ListView, AuthorizedOnlyDispatchMixin):
    """A view for viewing a list of comments owned by the user."""
    model = Comment
    template_name = 'authnapp/includes/my_comments_data.html'
    context_object_name = 'user_comments'
    paginate_by = 6

    def get_queryset(self):
        """Returns a queryset of user comments."""
        current_user = self.request.user
        return Comment.objects.filter(Q(user=current_user), Q(is_active=True))

    def get_context_data(self, **kwargs):
        """Adds the page title and user to the context and returns it."""
        context = super().get_context_data(**kwargs)
        context['title'] = 'Мои комментарии'
        context['habruserprofile'] = HabrUserProfile.objects.get(user=self.request.user)
        return context


class ToMeCommentsListView(ListView, AuthorizedOnlyDispatchMixin):
    """A view for viewing a list of comments sent in response to the user's
    content (to his articles or his comments)."""
    model = Comment
    template_name = 'authnapp/includes/comments_to_me_data.html'
    context_object_name = 'to_user_comments'
    paginate_by = 6

    def get_queryset(self):
        """Returns a queryset of comments sent in response to the user's content."""
        current_user = self.request.user
        return Comment.objects. \
            filter((Q(parent__user=current_user) | Q(comment_article__user=current_user)),
                   Q(is_active=True))  # В этом месте теперь ошибка

    def get_context_data(self, **kwargs):
        """Adds the page title and user to the context and returns it."""
        context = super().get_context_data(**kwargs)
        context['title'] = 'Комментарии к моему контенту'
        context['habruserprofile'] = HabrUserProfile.objects.get(user=self.request.user)
        return context


class SearchImageView(ListView, AuthorizedOnlyDispatchMixin):
    """View to display the search results for images (when using the site search bar).
    The search is performed by title of image or its part.
    """
    model = PictureFile
    template_name = 'authnapp/profile/profile-search-images.html'
    context_object_name = 'search_pictures'
    paginate_by = 3

    def get_queryset(self):
        """Returns a queryset filtered for the presence of a match with the data
        entered by the user.
        """
        query_phrase = self.request.GET.get('search_panel')
        if query_phrase:
            self.request.session['filter'] = query_phrase

        if 'filter' in self.request.session:
            query_phrase = self.request.session['filter']
        search_pictures = PictureFile.objects.filter(Q(file_name__icontains=query_phrase),
                                                     Q(user=self.request.user), Q(is_active=True)).order_by('file_name')
        print(self.request.session.session_key)
        return search_pictures

    def get_context_data(self, **kwargs):
        """Adds the page title and user to the context and returns it."""
        context = super().get_context_data(**kwargs)
        try:
            user_filter = self.request.session['filter']
            if user_filter == '':
                raise KeyError
            context['filter'] = user_filter
            context['title'] = f'Вы искали картинки по фразе "{user_filter}"'
        except KeyError:
            context['title'] = 'Чтобы выполнить поиск, задайте поисковую фразу!'
            self.request.session['filter'] = ''
        context['habruserprofile'] = HabrUserProfile.objects.get(user=self.request.user)
        return context


class MyMessagesListView(ListView, AuthorizedOnlyDispatchMixin):
    """A view for viewing a list of messages owned by the user."""
    model = Message
    template_name = 'authnapp/includes/my_messages_data.html'
    context_object_name = 'user_messages'
    paginate_by = 6

    def get_queryset(self):
        """Returns a queryset of user messages."""
        current_user = self.request.user
        return Message.objects.filter(Q(user=current_user), Q(is_active=True))

    def get_context_data(self, **kwargs):
        """Adds the page title and user to the context and returns it."""
        context = super().get_context_data(**kwargs)
        context['title'] = 'Мои сообщения'
        context['habruserprofile'] = HabrUserProfile.objects.get(user=self.request.user)
        return context


class MessageIsReadView(AuthorizedOnlyDispatchMixin, View):
    """View to mark messages as read or unread."""

    def get(self, request, **kwargs):
        """Identifies the desired message and places it read/unread.
        Returns to the page from which the user clicked."""
        message = Message.objects.get(slug=kwargs['slug'])
        message.is_read = not bool(message.is_read) if message.is_read else True
        message.save()
        try:
            if request.GET['source_page']:
                habruserprofile = HabrUserProfile.objects.get(user=self.request.user)
                return HttpResponseRedirect(reverse('auth:user_messages', args=[habruserprofile.uuid]))
        except MultiValueDictKeyError:
            return HttpResponseRedirect(reverse('moderation:moderation_calls'))
        return HttpResponseRedirect(reverse('index'))
