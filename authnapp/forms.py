"""
Contains forms for registration and authorization of the user, recovery of his password.
To manage the user's profile, forms are implemented in which he can change some of his data,
as well as change his avatar.
"""

import hashlib
import random

from django import forms
from django.contrib.auth.forms import (AuthenticationForm, UserChangeForm,
                                       UserCreationForm)
from django.core.exceptions import ValidationError
from django.forms import ClearableFileInput, formset_factory, BaseFormSet
from django.utils.translation import gettext_lazy as _

from articles.models import PictureFile
from authnapp.models import HabrUser, HabrUserProfile


class HabrUserLoginForm(AuthenticationForm):
    """A form for authorization of registered users.
    """
    error_messages = {
        'invalid_login': _(
            "Пожалуйста, введите верный логин и пароль с учетом регистра"
        ),
    }

    class Meta:
        """Defines the model involved and the fields used in the form.
        """
        model = HabrUser
        fields = ("username", "password")

    def __init__(self, *args, **kwargs):
        """Manages the classes of form fields for their correct display on the page.
        Sets placeholders for form fields.
        """
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = 'Введите имя пользователя'
        self.fields['password'].widget.attrs['placeholder'] = 'Введите пароль'
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control py-2 placeholders-text'


class HabrUserRegisterForm(UserCreationForm):
    """A form for registering new users."""

    class Meta:
        """Defines the model involved and the fields used in the form.
         """
        model = HabrUser
        fields = ("username", "first_name", "last_name", "password1", "password2", "email", "age", "avatar")

    def __init__(self, *args, **kwargs):
        """Manages the classes of form fields for their correct display on the page.
        Sets placeholders for form fields.
        """
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = 'Введите имя пользователя'
        self.fields['email'].widget.attrs['placeholder'] = 'Введите адрес эл.почты'
        self.fields['first_name'].widget.attrs['placeholder'] = 'Введите имя'
        self.fields['last_name'].widget.attrs['placeholder'] = 'Введите фамилию'
        self.fields['password1'].widget.attrs['placeholder'] = 'Введите пароль'
        self.fields['password2'].widget.attrs['placeholder'] = 'Подтвердите пароль'
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control py-2 placeholders-text'

    def clean_age(self):
        data = self.cleaned_data["age"]
        if data < 18:
            raise forms.ValidationError("Вы слишком молоды!")
        return data

    def save(self):
        user = super(HabrUserRegisterForm, self).save()

        user.is_active = False
        salt = hashlib.sha1(str(random.random()).encode("utf8")).hexdigest()[:6]
        user.activation_key = hashlib.sha1((user.email + salt).encode("utf8")).hexdigest()
        user.save()

        return user


class HabrUserEditForm(UserChangeForm):
    def __init__(self, *args, **kwargs):
        super(HabrUserEditForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs["class"] = "form-control"
            field.help_text = ""

    def clean_age(self):
        data = self.cleaned_data["age"]
        if data < 18:
            raise forms.ValidationError("Вы слишком молоды!")

        return data

    class Meta:
        model = HabrUser
        fields = ("username", "first_name", "email", "age", "avatar")


class HabrUserProfileEditForm(forms.ModelForm):
    class Meta:
        model = HabrUserProfile
        fields = ("tagline", "aboutMe", "gender")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs["class"] = "form-control"


class ImageBasePicturesForm(forms.ModelForm):
    """Base form for work with user images."""
    error_messages = {
        'invalid_file_name': _('Отсутствует один или несколько '
                               'заголовков для загружаемых картинок'),
        'too_short_file_name': _('Имя файла должно быть длиннее '
                                 '3 символов - вы ввели %(value)s'),
    }

    class Meta:
        """Defines the model involved and the fields used in the form.
        """
        model = PictureFile
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        """Manages the classes of form fields for their correct display on the page.
        Sets placeholder for 'file_name' form field.
        """
        super().__init__(*args, **kwargs)
        self.fields['file_name'].widget.attrs['placeholder'] = 'Введите заголовок картинки'
        self.fields['file_name'].widget.attrs['class'] = 'form-control'

    def clean(self):
        """Validation of form fields."""
        cleaned_data = super().clean()
        file_name = self.cleaned_data.get('file_name')
        if not file_name:
            msg = ValidationError(self.error_messages['invalid_file_name'],
                                  code='invalid_file_name')
            self.add_error('file_name', msg)

        return cleaned_data

    def clean_file_name(self):
        """Checks if file name isn't shorter than 4 characters."""
        file_name = self.cleaned_data.get('file_name')
        file_name_len = len(file_name)
        if file_name_len <= 3:
            msg = ValidationError(self.error_messages['too_short_file_name'],
                                  code='too_short_file_name',
                                  params={'value': file_name_len})
            self.add_error('file_name', msg)
        return file_name


class ImageLoadPicturesForm(ImageBasePicturesForm):
    """ A form for upload user images."""

    class Meta:
        """Defines the model involved and the fields used in the form.
        Assigns a widget for the 'picture_file' field and a js script attached to it.
        """
        model = PictureFile
        fields = ('file_name', 'picture_file')
        widgets = {'picture_file': ClearableFileInput(
            attrs={"name": "images", "onchange": "readURL(this);",
                   "type": "file", "accept": "image/*"})}

    def __init__(self, *args, **kwargs):
        """Manages the classes of form fields for their correct display on the page.
        """
        super().__init__(*args, **kwargs)
        self.fields['picture_file'].widget.attrs['class'] = 'form-control custom'


class ImageUpdatePicturesForm(ImageBasePicturesForm):
    """ A form for upload user images."""

    class Meta:
        """Defines the model involved and the fields used in the form.
        """
        model = PictureFile
        fields = ('file_name',)


class RequiredFormSet(BaseFormSet):
    """A formset class that forcibly calls validation of all forms included in it.
    Without this class, forms according to Django's provisions are not validated
    for the fullness of fields."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


# A formset for uploading images by the user.
ImageFormSet = formset_factory(
    ImageLoadPicturesForm, formset=RequiredFormSet, extra=1
)
