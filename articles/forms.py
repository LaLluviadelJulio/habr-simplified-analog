from django import forms
from articles.models import Article, Comment
from martor.fields import MartorFormField


class ArticleCreationForm(forms.ModelForm):
    body = MartorFormField()
    def __init__(self, *args, **kwargs):
        super(ArticleCreationForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs["class"] = "form-control"
    

    class Meta:
        model = Article 
        fields = ('article_category', 'title', 'body', 'tag')


class CommentCreateForm(forms.ModelForm):
    body = MartorFormField()
    def __init__(self, *args, **kwargs):
        super(CommentCreateForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs["class"] = "form-control"


    class Meta:
            model = Comment
            fields = ('body', 'parent')
