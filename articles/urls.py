from django.urls import path, include
from articles.views import ArticleView, CategoryView, TagArticleView, LikeToAuthorView, UserArticleView, CommentDeleteView, CommentLikeView
from .api import TagApi, ArticleApi, CommentApi, LikeApi, PictureFileApi, MessageApi
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register('tags', TagApi)
router.register('articles', ArticleApi, basename='Article')
router.register('comments', CommentApi)
router.register('likes', LikeApi)
router.register('picturefile', PictureFileApi)
router.register('message', MessageApi)

app_name = 'articles'
urlpatterns = [
    path('categories/<slug:category_slug>/', CategoryView.as_view(), name='category_read'),
    path('<slug:article_slug>/', ArticleView.as_view(), name='article_read'),
    path('tags/<slug:tag_slug>/', TagArticleView.as_view(), name='tag_articles'),
    path('like_article/<slug:slug>/', LikeToAuthorView.as_view(), name='article_like'),
    path('delete_comment/<slug:comment_id>/', CommentDeleteView.as_view(), name='comment_delete'),
    path('like_comment/<slug:comment_id>/', CommentLikeView.as_view(), name='comment_like'),
    path('user_articles/<slug:user_id>/', UserArticleView.as_view(), name='user_articles'),
    path('api/', include(router.urls)),
]
