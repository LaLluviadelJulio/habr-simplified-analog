from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from django.shortcuts import get_object_or_404
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from .serializers import TagSerializer, ArticleSerializer, CommentSerializer, LikeSerializer, PictureFileSerializer, \
    MessageSerializer
from .models import Tag, Article, Comment, Like, PictureFile, Message


class TagApi(ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    http_method_names = ['get', 'post', 'put', 'delete']
    authentication_classes = [BasicAuthentication]
    permission_classes = [AllowAny]

    permission_classes_by_action = {
        'list': [AllowAny],
        'retrieve': [AllowAny],
        'create': [IsAuthenticated],
        'update': [IsAuthenticated],
        'destroy': [IsAuthenticated]
    }

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def list(self, request):
        serializer = TagSerializer(self.queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        item = get_object_or_404(self.queryset, pk=pk)
        serializer = TagSerializer(item)
        return Response(serializer.data)

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, pk=None):
        instance = get_object_or_404(self.queryset, pk=pk)
        data = {
            "tag_name": request.POST.get('tag_name', None),
        }
        serializer = self.serializer_class(instance=instance,
                                           data=data,
                                           context={'tag_name': instance.tag_name},
                                           partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk, format=None):
        item = get_object_or_404(self.queryset, pk=pk)
        if item.is_active == True:
            item.is_active = False
            item.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CommentApi(ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    http_method_names = ['get', 'post', 'delete']
    authentication_classes = [BasicAuthentication]
    permission_classes = [AllowAny]

    permission_classes_by_action = {
        'list': [AllowAny],
        'retrieve': [AllowAny],
        'create': [IsAuthenticated],
        'destroy': [IsAuthenticated]
    }

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]


    def get_queryset(self):
        queryset = Comment.objects.all()
        article = self.request.query_params.get('article')
        if article is not None:
            queryset = queryset.filter(comment_article=article)
        else:
            queryset = None
        return queryset

    def list(self, request):
        serializer = CommentSerializer(self.get_queryset(), many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        item = get_object_or_404(self.queryset, pk=pk)
        serializer = CommentSerializer(item)
        return Response(serializer.data)

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, pk, format=None):
        item = get_object_or_404(self.queryset, pk=pk)
        if item.is_active:
          if item.user == request.user or request.user.is_superuser or request.user.is_staff:
                item.is_active = False
                item.save()
          else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_204_NO_CONTENT)



class ArticleApi(ModelViewSet):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
    http_method_names = ['get', 'post', 'put', 'delete']
    authentication_classes = [BasicAuthentication]
    permission_classes = [AllowAny]

    permission_classes_by_action = {
        'list': [AllowAny],
        'retrieve': [AllowAny],
        'create': [IsAuthenticated],
        'update': [IsAuthenticated],
        'destroy': [IsAuthenticated]
    }

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]


    def get_queryset(self):
        queryset = Article.objects.all()
        print(queryset)
        category = self.request.query_params.get('category')
        if category is not None:
            queryset = queryset.filter(article_category=category)
        return queryset

    def list(self, request):
        serializer = ArticleSerializer(self.get_queryset(), many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        item = get_object_or_404(self.queryset, pk=pk)
        serializer = ArticleSerializer(item)
        return Response(serializer.data)

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, pk=None):
        instance = get_object_or_404(self.queryset, pk=pk)
        data = {
            "title": request.POST.get('title', None),
            "body": request.POST.get('body', None),
            "user": request.POST.get('user', None),
        }
        serializer = self.serializer_class(instance=instance,
                                           data=data,
                                           context={'title': instance.title, 'body': instance.body},
                                           partial=True)
        if serializer.is_valid():
            if data['user'] == request.user or request.user.is_superuser or request.user.is_staff:
                serializer.save()
                return Response(data=serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk, format=None):
        item = get_object_or_404(self.queryset, pk=pk)
        if item.is_active:
          if item.user == request.user or request.user.is_superuser or request.user.is_staff:
                item.is_active = False
                item.save()
          else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_204_NO_CONTENT)


class PictureFileApi(ModelViewSet):
    serializer_class = PictureFileSerializer
    queryset = PictureFile.objects.all()
    http_method_names = ['get', 'post', 'put', 'delete']
    authentication_classes = [BasicAuthentication]
    permission_classes = [AllowAny]
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)

    permission_classes_by_action = {
        'list': [AllowAny],
        'retrieve': [AllowAny],
        'create': [IsAuthenticated],
        'destroy': [IsAuthenticated]
    }


    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]


    def get_queryset(self):

        queryset = PictureFile.objects.all()
        user = self.request.query_params.get('user')
        if user is not None:
            queryset = queryset.filter(user=user)
        return queryset

    def list(self, request, pk=None):
        serializer = PictureFileSerializer(self.get_queryset(), many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        item = get_object_or_404(self.queryset, pk=pk)
        serializer = PictureFileSerializer(item)
        return Response(serializer.data)

    def create(self, request):
        serializer = self.get_serializer(data=request.data)#, files=request.FILES
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, pk, format=None):
        item = get_object_or_404(self.queryset, pk=pk)
        if item.is_active:
          if item.user == request.user or request.user.is_superuser or request.user.is_staff:
                item.is_active = False
                item.save()
          else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_204_NO_CONTENT)


class LikeApi(ModelViewSet):
    serializer_class = LikeSerializer
    queryset = Like.objects.all()
    http_method_names = ['post', 'delete']
    authentication_classes = [BasicAuthentication]
    permission_classes_by_action = {
        'create': [IsAuthenticated],
        'destroy': [IsAuthenticated]
    }

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, pk, format=None):
        item = get_object_or_404(self.queryset, pk=pk)
        if item.is_active:
          if item.user == request.user or request.user.is_superuser or request.user.is_staff:
                item.is_active = False
                item.save()
          else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_204_NO_CONTENT)


class MessageApi(ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    http_method_names = ['get', 'post', 'delete']
    authentication_classes = [BasicAuthentication]
    permission_classes = [AllowAny]

    permission_classes_by_action = {
        'list': [AllowAny],
        'retrieve': [AllowAny],
        'create': [IsAuthenticated],
        'destroy': [IsAuthenticated]
    }

    def get_queryset(self):

        queryset = Message.objects.all()
        user = self.request.query_params.get('user')
        if user is not None:
            queryset = queryset.filter(user=user)
        return queryset

    def list(self, request):
        serializer = MessageSerializer(self.get_queryset(), many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        item = get_object_or_404(self.queryset, pk=pk)
        serializer = MessageSerializer(item)
        return Response(serializer.data)

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, pk, format=None):
        item = get_object_or_404(self.queryset, pk=pk)
        if item.is_active:
          if item.user == request.user or request.user.is_superuser or request.user.is_staff:
                item.is_active = False
                item.save()
          else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_204_NO_CONTENT)
