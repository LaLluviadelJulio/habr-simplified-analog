from rest_framework import serializers
from .models import Tag, Article, Comment, Like, PictureFile, Message


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class CommentSerializer(serializers.ModelSerializer):
    likes = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = '__all__'

    def get_likes(self, obj):
        return obj.get_likes()


class ArticleSerializer(serializers.ModelSerializer):
    tag = TagSerializer(many=True, read_only=True)
    likes = serializers.SerializerMethodField()
    comments = serializers.SerializerMethodField()

    class Meta:
        model = Article
        fields = '__all__'

    def get_likes(self, obj):
        return obj.get_likes()

    def get_comments(self, obj):
        return obj.get_comments()


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = '__all__'


class PictureFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = PictureFile
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
