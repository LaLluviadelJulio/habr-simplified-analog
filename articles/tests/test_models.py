from django.test import TestCase
from articles.models import Category, Tag, Article, Comment, MessageType, Message, Like, PictureFile
from authnapp.models import HabrUser
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.exceptions import ValidationError
from django.db import IntegrityError


class PictureModelTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = HabrUser.objects.create(
            username="testuser",
            first_name="testuser",
            last_name="testuser",
            email="testuser@ya.ru"
        )

        image_path = 'articles/tests/'
        image_name = 'Image0001.jpg'
        cls.pic1 = PictureFile.objects.create(
            file_name="test.jpg",
            slug="test.jpg",
            user=cls.user,
            picture_file=SimpleUploadedFile(name=image_name, content=open(f'{image_path}{image_name}', 'rb').read(),
                                            content_type='image/jpeg')
        )

    def test_it_type(self):
        self.assertIsInstance(self.pic1, PictureFile)


class CategoryModelTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.category = Category.objects.create(
            category_name="Category 1",
            slug="Category 1",
            is_active=True
        )

    def test_it_type(self):
        self.assertIsInstance(self.category, Category)


class TagModelTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.tag = Tag.objects.create(
            tag_name="Tag 1",
            slug="Tag 1"
        )

    def test_it_type(self):
        self.assertIsInstance(self.tag.tag_name, str)
        self.assertIsInstance(self.tag.slug, str)


class ArticleModelTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = HabrUser.objects.create(
            username="testuser",
            first_name="testuser",
            last_name="testuser",
            email="testuser@ya.ru"
        )

        cls.category = Category.objects.create(
            category_name="Category 1",
            slug="Category 21",
            is_active=True
        )

        cls.tag1 = Tag.objects.create(
            tag_name="Tag 1",
            slug="Tag 21"
        )

        cls.tag2 = Tag.objects.create(
            tag_name="Tag 2",
            slug="Tag 22"
        )

        cls.article = Article.objects.create(
            slug="Article 21",
            title="Title 1",
            body="Body 1",
            user=cls.user,
            article_category=cls.category
        )
        cls.article.tag.set((cls.tag1, cls.tag2))

    def test_it_type(self):
        self.assertIsInstance(self.article, Article)
        self.assertEqual(self.tag1.get_articles_count(), 1, 'Неверное количество статей тэга')

    def test_validator(self):
        with self.assertRaises(ValidationError):
            Article.body.field.run_validators(value="Body 1")
        self.assertIsNone(Article.body.field.run_validators(
            value="Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 "))

        with self.assertRaises(ValidationError):
            Article.title.field.run_validators(value="Title 1")
        self.assertIsNone(Article.title.field.run_validators(value="Title 1Title 1Title 1Title 1Title 1Title 1Title 1"))


class CommentModelTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = HabrUser.objects.create(
            username="testuser",
            first_name="testuser",
            last_name="testuser",
            email="testuser@ya.ru"
        )

        cls.category = Category.objects.create(
            category_name="Category 71",
            slug="Category 31",
            is_active=True
        )

        cls.tag1 = Tag.objects.create(
            tag_name="Tag 317",
            slug="Tag 317"
        )

        cls.tag2 = Tag.objects.create(
            tag_name="Tag 327",
            slug="Tag 327"
        )

        cls.article = Article.objects.create(
            slug="Article 314",
            title="Title 175",
            body="Body 1",
            user=cls.user,
            article_category=cls.category
        )
        cls.article.tag.set((cls.tag1, cls.tag2))

        cls.comment1 = Comment.objects.create(
            body="Body 1",
            user=cls.user,
            slug="Comment 21",
            comment_article=cls.article
        )

        cls.comment2 = Comment.objects.create(
            body="Body 2",
            user=cls.user,
            slug="Comment 23",
            comment_article=cls.article,
            parent=cls.comment1
        )

    def test_it_type(self):
        self.assertIsInstance(self.comment1, Comment)
        self.assertIsInstance(self.comment2, Comment)
        self.assertEqual(self.article.get_comments(), 2, 'Неверное количество комментариев')

    def test_validator(self):
        with self.assertRaises(ValidationError):
            Comment.body.field.run_validators(value="Body")
        self.assertIsNone(Comment.body.field.run_validators(
            value="Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 Body 1 "))


class MessageTypeModelTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.message_type = MessageType.objects.create(
            message_type_name="MT 1",
            text="Text 1",
            slug="MT21",
        )

    def test_it_type(self):
        self.assertIsInstance(self.message_type, MessageType)


class MessageModelTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.message_type = MessageType.objects.create(
            message_type_name="MT 13",
            text="Text 13",
            slug="MT21",
        )

        cls.user = HabrUser.objects.create(
            username="testuser",
            first_name="testuser",
            last_name="testuser",
            email="testuser@ya.ru"
        )

        cls.message = Message.objects.create(
            text="Text 14",
            user=cls.user,
            slug="M 21",
            message_type=cls.message_type
        )

    def test_it_type(self):
        self.assertIsInstance(self.message, Message)


class LikeModelTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = HabrUser.objects.create(
            username="testuser",
            first_name="testuser",
            last_name="testuser",
            email="testuser@ya.ru"
        )

        cls.user2 = HabrUser.objects.create(
            username="testuser2",
            first_name="testuser2",
            last_name="testuser2",
            email="testuser2@ya.ru"
        )

        cls.category = Category.objects.create(
            category_name="Category 81",
            slug="Category 81",
            is_active=True
        )

        cls.tag1 = Tag.objects.create(
            tag_name="Tag 18",
            slug="Tag 18"
        )

        cls.tag2 = Tag.objects.create(
            tag_name="Tag 28",
            slug="Tag 28"
        )

        cls.article = Article.objects.create(
            slug="Article 18s",
            title="Title 1s8",
            body="Body 1",
            user=cls.user,
            article_category=cls.category
        )
        cls.article.tag.set((cls.tag1, cls.tag2))

        cls.comment = Comment.objects.create(
            body="Body 1",
            user=cls.user,
            slug="Comment 21",
            comment_article=cls.article
        )

        cls.like1 = Like.objects.create(
            user=cls.user,
            slug="C2",
            like_article=cls.article,
            like_user=cls.user2
        )

        cls.like2 = Like.objects.create(
            user=cls.user,
            slug="C21",
            like_comment=cls.comment,
            like_user=cls.user2
        )

    def test_it_type(self):
        self.assertIsInstance(self.like1, Like)
        self.assertIsInstance(self.like2, Like)
        self.assertEqual(self.article.get_likes(), 1, 'Неверное количество лайков статьи')
        self.assertEqual(self.comment.get_likes(), 1, 'Неверное количество лайков коментария')

