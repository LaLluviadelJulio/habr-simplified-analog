from rest_framework.test import RequestsClient
from django.test import TestCase
from rest_framework.test import APIClient
from requests.auth import HTTPBasicAuth
from django.core.files.uploadedfile import SimpleUploadedFile
from authnapp.models import HabrUser
from articles.models import Tag, Category, Article, Comment, Like, PictureFile, Message, MessageType

username = 'admin'
password = 'dfv68CC$'


class TagTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = HabrUser.objects.create(
            username=username,
            first_name=username,
            is_superuser=True,
            last_name=username,
            email=f'{username}@mail.ru'
        )
        cls.user.set_password(password)
        cls.user.save()

    def test_tag_get(self):
        client = RequestsClient()
        response = client.get('http://127.0.0.1:8000/articles/api/tags/')
        assert response.status_code == 200

    def test_tag_create(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {'tag_name': 'Tag 1',
                'slug': 'tag1'}
        response = client.post('http://127.0.0.1:8000/articles/api/tags/', data)
        assert response.status_code == 201

    def test_tag_retrieve(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {'tag_name': 'Tag 1',
                'slug': 'tag1'}
        response_post = client.post('http://127.0.0.1:8000/articles/api/tags/', data)
        client = RequestsClient()
        id = response_post.json()['id']
        response_get = client.get(f'http://127.0.0.1:8000/articles/api/tags/{id}/')
        assert response_get.status_code == 200

    def test_tag_destroy(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {'tag_name': 'Tag 2',
                'slug': 'tag2',
                'is_active': True}
        response_post = client.post('http://127.0.0.1:8000/articles/api/tags/', data)
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        id = response_post.json()['id']
        response_destroy = client.delete(f'http://127.0.0.1:8000/articles/api/tags/{id}/')
        assert response_destroy.status_code == 204


class ArticleTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = HabrUser.objects.create(
            username=username,
            first_name=username,
            is_superuser=True,
            last_name=username,
            email=f'{username}@mail.ru'
        )
        cls.user.set_password(password)
        cls.user.save()
        cls.tag = Tag.objects.create(
            tag_name="Tag 1",
            slug="Tag 21"
        )
        cls.category = Category.objects.create(
            category_name="Category 1",
            slug="Category 21",
            is_active=True
        )

    def test_article_get(self):
        client = RequestsClient()
        response = client.get('http://127.0.0.1:8000/articles/api/articles/')
        assert response.status_code == 200

    def test_article_create(self):
        id = self.tag.id
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {"title": "Title Title Title",
                "slug": "title1",
                "tag": [{"id":id}],
                "user": f"{str(self.user.uuid)}",
                "article_category": self.category.id,
                "body": "Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body"}
        response = client.post('http://127.0.0.1:8000/articles/api/articles/', data)
        assert response.status_code == 201

    def test_article_update(self):
        id = self.tag.id
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {"title": "Title Title Title",
                "slug": "title1",
                "tag": [{"id":id}],
                "user": f"{str(self.user.uuid)}",
                "article_category": self.category.id,
                "body": "Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body"}
        response = client.post('http://127.0.0.1:8000/articles/api/articles/', data)
        article_id = response.json()['id']
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {"id": article_id,
                "title": "Title Title Title 333",
                "slug": "title1",
                "tag": [{"id":id}],
                "user": f"{str(self.user.uuid)}",
                "article_category": self.category.id,
                "body": "Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body"}
        response = client.put(f'http://127.0.0.1:8000/articles/api/articles/{article_id}/', data)
        assert response.status_code == 201



    def test_article_retrieve(self):
        id = self.tag.id
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {"title": "Title Title Title",
                "slug": "title1",
                "tag": [{"id": id}],
                "user": f"{str(self.user.uuid)}",
                "article_category": self.category.id,
                "body": "Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body"}
        response = client.post('http://127.0.0.1:8000/articles/api/articles/', data)
        client = RequestsClient()
        id = response.json()['id']
        response_get = client.get(f'http://127.0.0.1:8000/articles/api/articles/{id}/')
        assert response_get.status_code == 200

    def test_tag_destroy(self):
        id = self.tag.id
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {"title": "Title Title Title",
                "slug": "title1",
                "tag": [{"id": id}],
                "user": f"{str(self.user.uuid)}",
                "article_category": self.category.id,
                "is_active": True,
                "body": "Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body"}
        response = client.post('http://127.0.0.1:8000/articles/api/articles/', data)
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        id = response.json()['id']
        response_destroy = client.delete(f'http://127.0.0.1:8000/articles/api/articles/{id}/')
        assert response_destroy.status_code == 204



class CommentTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = HabrUser.objects.create(
            username=username,
            first_name=username,
            is_superuser=True,
            last_name=username,
            email=f'{username}@mail.ru'
        )
        cls.user.set_password(password)
        cls.user.save()
        cls.tag1 = Tag.objects.create(
            tag_name="Tag 1",
            slug="Tag 21"
        )
        cls.tag2 = Tag.objects.create(
            tag_name="Tag 2",
            slug="Tag 22"
        )
        cls.category = Category.objects.create(
            category_name="Category 1",
            slug="Category 21",
            is_active=True
        )
        cls.article = Article.objects.create(
            slug="Article 21",
            title="Title 1",
            body="Body 1",
            user=cls.user,
            article_category=cls.category
        )
        cls.article.tag.set((cls.tag1, cls.tag2))

        cls.comment1 = Comment.objects.create(
            body="Body 1",
            user=cls.user,
            slug="Comment 21",
            comment_article=cls.article
        )

    def test_comment_get(self):
        client = RequestsClient()
        response = client.get(f'http://127.0.0.1:8000/articles/api/comments/?article={self.article.id}')
        assert response.status_code == 200

    def test_comment_create(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {"comment_article": self.article.id,
                "slug": "title1",
                "user": f"{str(self.user.uuid)}",
                "body": " Body  Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body Body"}
        response = client.post('http://127.0.0.1:8000/articles/api/comments/', data)
        assert response.status_code == 201

    def test_comment_retrieve(self):
        client = RequestsClient()
        response = client.get(f'http://127.0.0.1:8000/articles/api/comments/{self.comment1.id}')
        assert response.status_code == 200

    def test_comment_destroy(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        response = client.delete(f'http://127.0.0.1:8000/articles/api/comments/{self.comment1.id}')
        assert response.status_code == 204



class LikeTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = HabrUser.objects.create(
            username=username,
            first_name=username,
            last_name=username,
            email=f'{username}@mail.ru'
        )

        cls.user.set_password(password)
        cls.user.save()
        cls.tag1 = Tag.objects.create(
            tag_name="Tag 1",
            slug="Tag 21"
        )
        cls.tag2 = Tag.objects.create(
            tag_name="Tag 2",
            slug="Tag 22"
        )
        cls.category = Category.objects.create(
            category_name="Category 1",
            slug="Category 21",
            is_active=True
        )
        cls.article = Article.objects.create(
            slug="Article 21",
            title="Title 1",
            body="Body 1",
            user=cls.user,
            article_category=cls.category
        )
        cls.article.tag.set((cls.tag1, cls.tag2))

        cls.comment1 = Comment.objects.create(
            body="Body 1",
            user=cls.user,
            slug="Comment 21",
            comment_article=cls.article
        )

        cls.like1 = Like.objects.create(
            user=cls.user,
            slug="C2",
            is_active=True,
            like_article=cls.article,
            like_user=cls.user
        )

    def test_like_article_create(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {"like_article": self.article.id,
                "slug": "like1",
                "user": f"{str(self.user.uuid)}",
                "like_user": f"{str(self.user.uuid)}"}
        response = client.post('http://127.0.0.1:8000/articles/api/likes/', data)
        assert response.status_code == 201

    def test_like_comment_create(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {"like_comment": self.comment1.id,
                "slug": "like1",
                "user": f"{str(self.user.uuid)}",
                "like_user": f"{str(self.user.uuid)}"}
        response = client.post('http://127.0.0.1:8000/articles/api/likes/', data)
        assert response.status_code == 201

    def test_like_destroy(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        response = client.delete(f'http://127.0.0.1:8000/articles/api/likes/{self.like1.id}/')
        assert response.status_code == 204


class PictureFileTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = HabrUser.objects.create(
            username=username,
            first_name=username,
            is_superuser=True,
            last_name=username,
            email=f'{username}@mail.ru'
        )
        cls.user.set_password(password)
        cls.user.save()

        image_path = 'articles/tests/'
        image_name = 'Image0001.jpg'
        cls.pic1 = PictureFile.objects.create(
            file_name="test.jpg",
            slug="test.jpg",
            user=cls.user,
            picture_file=SimpleUploadedFile(name=image_name, content=open(f'{image_path}{image_name}', 'rb').read(),
                                            content_type='image/jpeg')
        )


    def test_picturefile_get(self):
        client = RequestsClient()
        response = client.get(f'http://127.0.0.1:8000/articles/api/picturefile/')
        assert response.status_code == 200

    def test_picturefile_create(self):
        client = APIClient()
        #user = User.objects.get(username=username)
        client.force_authenticate(user=self.user)
        image_path = 'articles/tests/'
        image_name = 'Image0001.jpg'
        picture =SimpleUploadedFile(name=image_name, content=open(f'{image_path}{image_name}', 'rb').read(),
                                            content_type='image/jpeg')
        data = {"file_name": "file1.jpg",
                "slug": "pic1",
                "user": f"{str(self.user.uuid)}",
                "picture_file": picture}
        response = client.post('http://127.0.0.1:8000/articles/api/picturefile/', data, format='multipart')
        assert response.status_code == 201

    def test_picturefile_destroy(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        response = client.delete(f'http://127.0.0.1:8000/articles/api/picturefile/{self.pic1.id}/')
        assert response.status_code == 204


class MessageTest(TestCase):
    @classmethod
    def setUp(cls):
        cls.user = HabrUser.objects.create(
            username=username,
            first_name=username,
            is_superuser=True,
            last_name=username,
            email=f'{username}@mail.ru'
        )
        cls.user.set_password(password)
        cls.user.save()

        cls.message_type = MessageType.objects.create(
            message_type_name="Type 1"
        )

        cls.message = Message.objects.create(
            message_type=cls.message_type,
            slug="message",
            text="text",
            user=cls.user
        )

    def test_message_get(self):
        client = RequestsClient()
        response = client.get(f'http://127.0.0.1:8000/articles/api/message/')
        assert response.status_code == 200

    def test_message_create(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        data = {"message_type": self.message_type.id,
                "text": "message",
                "slug": "message",
                "is_active": True,
                "user": f"{str(self.user.uuid)}"}
        response = client.post('http://127.0.0.1:8000/articles/api/message/', data)
        assert response.status_code == 201

    def test_message_destroy(self):
        client = RequestsClient()
        client.auth = HTTPBasicAuth(username, password)
        response = client.delete(f'http://127.0.0.1:8000/articles/api/message/{self.message.id}/')
        assert response.status_code == 204