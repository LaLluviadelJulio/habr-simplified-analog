"""
Contains unit and integration tests for checking the views of the web application.
"""

import logging
import sys
from datetime import datetime, timedelta

from django.db.models import Q
from django.template.defaultfilters import slugify
from django.test import TestCase, Client
from django.urls import reverse

from articles.models import Category, Article, Tag
from authnapp.models import HabrUser

if len(sys.argv) > 1 and sys.argv[1] == 'test':
    logging.disable(logging.CRITICAL)


class TestArticleBase(TestCase):
    """Basic test class for a single setUp and keeping the DRY principle."""

    def setUp(self):
        """Creating and preparing a user, categories, tags and articles test objects."""
        self.client = Client()
        self.test_user_01 = HabrUser.objects.create_user(username='QueenTeen',
                                                         first_name='Quentin',
                                                         last_name='Tarantino',
                                                         email='queenteen@feet.ru',
                                                         is_active=True)
        self.test_user_01.set_password('QweRtyN')
        self.test_user_01.save()

        self.test_category_01 = Category.objects.create(category_name='Strange movies',
                                                        slug='strange_movies')
        self.test_category_02 = Category.objects.create(category_name='Bloody movies',
                                                        slug='bloody_movies')

        self.test_tag_01 = Tag.objects.create(tag_name='feet', slug='feet')
        self.test_tag_02 = Tag.objects.create(tag_name='blood', slug='blood')

        for number in range(1, 8):
            article = Article.objects.create(title=f'{number}' * 3,
                                             body=f'{number}' * 10,
                                             is_active=True,
                                             user=self.test_user_01,
                                             registration_time=datetime.now() - timedelta(hours=number))
            article.slug = slugify(article.title)
            article.tag.add(self.test_tag_01, self.test_tag_02)

            article.category = self.test_category_02 if number % 2 == 0 else self.test_category_01
            article.save()


class TestMainView(TestArticleBase):
    """Testing MainView."""

    def test_is_response_status_code_correct(self):
        """Checks that the server response code is 200."""
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_is_template_correct(self):
        """Checks that the view uses the correct template."""
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'articles/index.html')

    def test_is_view_url_accessible_by_name(self):
        """Checks if the view URL is available by name."""
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

    def test_is_view_uses_context_object_name(self):
        """Checks that the specified context object name is used."""
        response = self.client.get('/')
        self.assertTrue('all_articles' in response.context)

    def test_does_menu_have_only_active_categories(self):
        """Checks that the menu contains active categories."""
        response = self.client.get('/')
        categories = Category.objects.filter(is_active=True)
        for categories_set in zip(response.context['category_menu_items'], categories):
            self.assertEqual(*categories_set)

    def test_is_paginated(self):
        """Checks that pagination is being performed."""
        response = self.client.get('/')
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue(len(response.context['all_articles']) == 3)

        response = self.client.get('/' + '?page=3')
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue(len(response.context['all_articles']) == 1)

    def test_do_articles_of_all_categories_fall_into_queryset(self):
        """Checks that articles of all categories fall into the queryset."""
        response = self.client.get('/')
        view_categories = {article.category for article in response.context['all_articles']}
        all_active_categories = Category.objects.filter(is_active=True)
        for categories_set in zip(view_categories, all_active_categories):
            self.assertEqual(*categories_set)

    def test_are_articles_ranked_by_date_of_addition(self):
        """Checks that the articles in the page output are ranked by the date of addition."""
        all_articles = []
        for page in range(1, 4):
            response = self.client.get('/' + f'?page={page}')
            all_articles.extend(response.context['all_articles'])

        for index in range(1, len(all_articles)):
            article_01_date, article_02_date = all_articles[index - 1].registration_time, \
                                               all_articles[index].registration_time
            self.assertTrue(article_01_date > article_02_date)

    def test_does_queryset_contain_only_active_articles(self):
        """Checks that only articles with the 'is_active' field = True gets into the queryset."""
        response = self.client.get('/')
        articles = Article.objects.filter(is_active=True)
        for articles_set in zip(response.context['all_articles'], articles):
            self.assertEqual(*articles_set)

    def test_no_inactive_articles_in_queryset(self):
        """Checks that inactive articles do not fall into the queryset."""
        article = Article.objects.get(id=1)
        response = self.client.get('/')
        self.assertIn(article, response.context['all_articles'])

        article.is_active = False
        article.save()
        response = self.client.get('/')
        self.assertNotIn(article, response.context['all_articles'])


class TestCategoryView(TestArticleBase):
    """Testing CategoryView."""

    def test_is_response_status_code_correct(self):
        """Checks that the server response code is 200."""
        response = self.client.get(f'/articles/categories/{self.test_category_01.slug}/')
        self.assertEqual(response.status_code, 200)

    def test_is_template_correct(self):
        """Checks that the view uses the correct template."""
        response = self.client.get(f'/articles/categories/{self.test_category_01.slug}/')
        self.assertTemplateUsed(response, 'articles/read_category.html')

    def test_is_view_url_accessible_by_name(self):
        """Checks if the view URL is available by name."""
        response = self.client.get(reverse('articles:category_read',
                                           args=[self.test_category_01.slug]))
        self.assertEqual(response.status_code, 200)

    def test_is_view_uses_context_object_name(self):
        """Checks that the specified context object name is used."""
        response = self.client.get(reverse('articles:category_read',
                                           args=[self.test_category_01.slug]))
        self.assertTrue('category_articles' in response.context)

    def test_is_paginated(self):
        """Checks that pagination is being performed."""
        response = self.client.get(f'/articles/categories/{self.test_category_01.slug}/')
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue(len(response.context['category_articles']) == 3)

        response = self.client.get(f'/articles/categories/{self.test_category_01.slug}/'
                                   + '?page=2')
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'])
        self.assertTrue(len(response.context['category_articles']) == 1)

    def test_do_articles_of_current_category_fall_into_queryset(self):
        """Checks that articles of only current category fall into the queryset."""
        response = self.client.get(f'/articles/categories/{self.test_category_01.slug}/')
        view_categories = {article.category for article in response.context['category_articles']}
        current_category = Category.objects.get(slug=self.test_category_01.slug)
        self.assertEqual(*view_categories, current_category)

    def test_are_articles_ranked_by_date_of_addition(self):
        """Checks that the articles in the page output are ranked by the date of addition."""
        category_articles = []
        for page in range(1, 3):
            response = self.client.get(f'/articles/categories/{self.test_category_01.slug}/'
                                       + f'?page={page}')
            category_articles.extend(response.context['category_articles'])

        for index in range(1, len(category_articles)):
            article_01_date, article_02_date = category_articles[index - 1].registration_time, \
                                               category_articles[index].registration_time
            self.assertTrue(article_01_date > article_02_date)

    def test_does_queryset_contain_only_active_articles(self):
        """Checks that only articles with the 'is_active' field = True gets into the queryset."""
        response = self.client.get(f'/articles/categories/{self.test_category_01.slug}/')
        articles = Article.objects.filter(Q(is_active=True), Q(category=self.test_category_01))
        for articles_set in zip(response.context['category_articles'], articles):
            self.assertEqual(*articles_set)

    def test_no_inactive_articles_in_queryset(self):
        """Checks that inactive articles do not fall into the queryset."""
        article = Article.objects.get(id=1)
        response = self.client.get(f'/articles/categories/{self.test_category_01.slug}/')
        self.assertIn(article, response.context['category_articles'])

        article.is_active = False
        article.save()
        response = self.client.get(f'/articles/categories/{self.test_category_01.slug}/')
        self.assertNotIn(article, response.context['category_articles'])

    def test_does_view_use_correct_title(self):
        """Checks that the view uses the correct title."""
        response = self.client.get(f'/articles/categories/{self.test_category_01.slug}/')
        self.assertEqual(response.context['title'], f'Посты категории {self.test_category_01}')


class TestArticleView(TestArticleBase):
    """Testing ArticleView."""

    def setUp(self):
        super().setUp()
        self.article = Article.objects.get(id=1)

    def test_is_response_status_code_correct(self):
        """Checks that the server response code is 200."""
        response = self.client.get(f'/articles/{self.article.slug}/')
        self.assertEqual(response.status_code, 200)

    def test_is_template_correct(self):
        """Checks that the view uses the correct template."""
        response = self.client.get(f'/articles/{self.article.slug}/')
        self.assertTemplateUsed(response, 'articles/read-article.html')

    def test_is_view_url_accessible_by_name(self):
        """Checks if the view URL is available by name."""
        response = self.client.get(reverse('articles:article_read',
                                           args=[self.article.slug]))
        self.assertEqual(response.status_code, 200)

    def test_is_selected_article_displayed(self):
        """Checks that the specified article is displayed on the page."""
        response = self.client.get(reverse('articles:article_read',
                                           args=[self.article.slug]))
        self.assertEqual(response.context['article'], self.article)
