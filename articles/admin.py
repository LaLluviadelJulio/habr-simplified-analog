"""Provides package integration into the admin panel."""
from django.contrib import admin
from django.forms import Textarea, ModelForm
from django_mptt_admin.admin import DjangoMpttAdmin

from articles.models import Category, Tag, Article, Comment, Like, Message, MessageType, PictureFile


class BigCommentBodyForm(ModelForm):
    """A form for the comments' admin panel that uses a more convenient widget for large text."""

    class Meta:
        """Defining a widget for a field."""
        model = Comment
        widgets = {
            'body': Textarea
        }
        fields = '__all__'


class BigMessageTextForm(ModelForm):
    """A form for the messages' admin panel that uses a more convenient widget for large text."""

    class Meta:
        """Defining a widget for a field."""
        model = Message
        widgets = {
            'text': Textarea
        }
        fields = '__all__'


class BigArticleBodyForm(ModelForm):
    """A form for the articles' admin panel that uses a more convenient widget for large text."""

    class Meta:
        """Defining a widget for a field."""
        model = Article
        widgets = {
            'body': Textarea
        }
        fields = '__all__'


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'category_name', 'is_active')
    list_display_links = ('id', 'category_name')
    search_fields = ('title', 'is_active')
    exclude = ('slug',)


class ArticleAdmin(admin.ModelAdmin):
    form = BigArticleBodyForm
    list_display = ('id', 'title', 'is_active', 'create_time', 'user', 'article_category')
    list_display_links = ('id', 'title',)
    search_fields = ('title', 'is_active')
    exclude = ('slug',)


class TagAdmin(admin.ModelAdmin):
    list_display = ('id', 'tag_name')
    list_display_links = ('id', 'tag_name',)
    search_fields = ('tag_name',)
    exclude = ('slug',)


class MessageTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'message_type_name')
    list_display_links = ('id', 'message_type_name',)
    search_fields = ('message_type_name',)
    exclude = ('slug',)


class MessageAdmin(admin.ModelAdmin):
    form = BigMessageTextForm
    list_display = ('id', 'user', 'get_cropped_text')
    list_display_links = ('id', 'get_cropped_text',)
    search_fields = ('text', 'user')
    exclude = ('slug',)


class CommentAdmin(DjangoMpttAdmin):
    form = BigCommentBodyForm
    list_display = ('id', 'body',)
    list_display_links = ('id', 'body',)
    search_fields = ('body',)
    prepopulated_fields = {"slug": ("body",)}


class LikeAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'like_article', 'like_comment', 'like_user')
    list_display_links = ('id', 'user', 'like_article', 'like_comment', 'like_user')
    search_fields = ('user', 'like_article', 'like_comment', 'like_user')
    exclude = ('slug', 'like_user')


class PictureFileAdmin(admin.ModelAdmin):
    """A class for working in the admin panel with instances of the PictureFile model."""
    list_display = ('id', 'user', 'file_name', 'image_tag')
    list_display_links = ('id', 'user', 'file_name',)
    search_fields = ('user', 'file_name')
    exclude = ('slug',)
    fields = ('user', 'file_name', 'picture_file', 'image_tag')
    readonly_fields = ['image_tag']
    actions = ['delete_one_at_a_time']

    def get_actions(self, request):
        """Removing the standard method of mass deletion of objects."""
        actions = super().get_actions(request)
        del actions['delete_selected']
        return actions

    def delete_one_at_a_time(self, request, queryset):
        """Custom method for deleting objects one by one
        (with calling the standard delete model method)"""
        for picture_object in queryset:
            picture_object.delete()
        number_of_images = queryset.count()
        message = 'Удалена 1 картинка' if number_of_images == 1 \
            else f'Удалено картинок: {number_of_images}'
        self.message_user(request, message)

    delete_one_at_a_time.short_description = "Удаление выбранных картинок"


admin.site.register(Category, CategoryAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(MessageType, MessageTypeAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Like, LikeAdmin)
admin.site.register(PictureFile, PictureFileAdmin)
