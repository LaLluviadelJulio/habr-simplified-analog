"""The submodule contains views for working with objects of
articles and categories (sections of the site).
Within this submodule, work is performed with the main entities of the project:
the formation of a set of articles visible to the user or a separate article.

Here are views:

    * to display the main page of the site (to display all articles);
    * to display all articles of the category;
    * to display a separate article;
"""
import logging
from datetime import datetime, timedelta

from bs4 import BeautifulSoup
from django import forms
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db.models import Count, Q
from django.http import HttpResponseForbidden
from django.shortcuts import HttpResponseRedirect, get_object_or_404, render
from django.urls import reverse
from django.views import View
from django.views.generic import DetailView, ListView, DeleteView
from django.views.generic.edit import FormMixin
from martor.utils import markdownify

from articles.forms import ArticleCreationForm, CommentCreateForm
from articles.models import (Article, Category, Comment, Like, Message,
                             MessageType, Tag)
from authnapp.models import HabrUser

logger = logging.getLogger(__name__)

COMMENTS_BAN = datetime.now() + timedelta(days=14)


def help_view(request):
    return render(request, "articles/help.html")


def page_not_found_view(request, exception):
    """View for the 404 page."""
    context = {}
    context['title'] = '404: Страница не найдена'
    response = render(request, 'articles/misc/404.html', context=context)
    response.status_code = 404
    return response


class MainView(ListView):
    """View for the main page."""
    model = Article
    template_name = 'articles/index.html'
    context_object_name = 'all_articles'
    paginate_by = 5

    def get_queryset(self):
        """Displaying all active articles.
         Articles are displayed in an orderly manner, starting from the most recent."""
        return Article.objects.filter(is_active='ACTIVE'). \
            select_related('user', 'article_category'). \
            only('slug', 'title', 'body', 'create_time',
                 'article_category__slug', 'article_category__category_name',
                 'user__uuid', 'user__username')

    def get_context_data(self, *, object_list=None, **kwargs):
        """Calculates the frequency of occurrence of tags in articles
         and puts in context the 20 most popular tags to display them in the tag cloud."""
        context = super().get_context_data()
        temp_article_queryset = Article.objects.filter(is_active='ACTIVE').values('tag'). \
            annotate(tag_count=Count('tag', distinct=True))
        temp_article_queryset.group_by = ['tag']
        temp_article_queryset = temp_article_queryset.values('tag', 'tag_count'). \
            order_by('-tag_count').values('tag')
        context['cloud_tags'] = Tag.objects.filter(id__in=temp_article_queryset)[:20]
        return context


class CategoryView(ListView):
    """View for articles of a separate category (section)."""
    model = Article
    template_name = 'articles/read_category.html'
    paginate_by = 5
    slug_url_kwarg = 'category_slug'
    category = ''

    def get_queryset(self):
        """Returns a queryset of active articles of the specified category."""
        self.category = Category.objects.get(slug=self.kwargs.get('category_slug'))
        return Article.objects.filter(Q(article_category=self.category), Q(is_active='ACTIVE')). \
            select_related('user', 'article_category').only('slug', 'title', 'body', 'create_time',
                                                            'article_category__slug', 'article_category__category_name',
                                                            'user__uuid', 'user__username')

    def get_context_data(self, *args, **kwargs):
        """Puts the title in the context according to the selected category."""
        context = super().get_context_data(**kwargs)
        context['title'] = f'Посты категории {self.category}'
        return context


class ArticleForm(forms.Form):
    message = forms.CharField()


class ArticleView(FormMixin, DetailView):
    """View for the output of a separate article."""
    model = Article
    template_name = 'articles/read-article.html'
    slug_url_kwarg = 'article_slug'
    form_class = ArticleForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['is_like'] = Like.objects.all().filter(user=self.request.user,
                                                           like_article=self.object.id).count()
        context['is_authenticated'] = self.request.user.is_authenticated
        if context['article'].user.username in self.request.session.keys():
            context['author_is_liked'] = True
        context['is_staff'] = self.request.user.is_staff
        return context

    def get_success_url(self):
        return reverse('author-article_read', kwargs={'slug': self.object.slug})

    def get(self, request, *args, **kwargs):
        """Receives comments and adds context."""
        article = self.object = self.get_object()
        comments = Comment.objects.filter(comment_article=
                                          article.id).select_related('comment_article')
        context = self.get_context_data(**kwargs)
        comment_form = CommentCreateForm()
        context.update({'article': article, 'comments': comments,
                        'comment_form': comment_form})
        return render(request, 'articles/read-article.html', context=context)

    def post(self, request, *args, **kwargs):
        """Creates a comment added by the user, processes the user's call to the comment, adds/removes a like."""
        if 'body' in request.POST:
            self.create_comment(request)
        if 'type' in request.GET:
            self.create_like_or_deny(request)
        return HttpResponseRedirect(request.path)

    def create_comment(self, request):
        """Creates a new comment.
        Initiates a user call if the comment contains usernames of registered
        users in the form of a call."""
        comment_form = CommentCreateForm(request.POST)
        if comment_form.is_valid():
            post = comment_form.save(commit=False)
            post.comment_article = self.get_object()
            post.save()

            users = self.find_nicknames_in_markdown(post.body)
            [self.message_call_create(post, username) for username in users]

    def create_like_or_deny(self, request):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        self.object = self.get_object()
        form = self.get_form()
        message_type = request.GET['type']
        if message_type == 'like':
            likes = Like.objects.all().filter(user=request.user, like_article=self.object.id).count()
            if likes == 0:
                Like.objects.create(
                    user=request.user,
                    like_article=self.object
                )
            else:
                like = Like.objects.get(user=request.user, like_article=self.object.id)
                like.delete()
        elif message_type == 'deny':
            self.object.is_active = 'DENY'
            self.object.save()
            try:
                message_sys_type, _ = MessageType.objects.get_or_create(
                    message_type_name='Снятие с публикации', text='Снятие с публикации', sys_type='DENY')
                Message.objects.create(message_type=message_sys_type,
                                       user=self.object.user,
                                       text=f'Ваша статья "{self.object.title}" снята с публикации.')
            except ObjectDoesNotExist:
                pass
            return HttpResponseRedirect(reverse("index"))
        elif message_type == 'delete':
            self.object.is_active = 'USRDEL'
            self.object.save()
            return HttpResponseRedirect(reverse("index"))
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        return super().form_valid(form)

    def find_nicknames_in_markdown(self, markdown_text):
        """Finds the nicknames of users when they are called in the comments."""
        marktext = markdownify(markdown_text)
        soup = BeautifulSoup(marktext, 'html.parser')
        return [username.text[1:] for username in soup.findAll('a', {'class': 'direct-mention-link'})]

    def message_call_create(self, comment, username):
        """Creates a new message for the user when he is called in the comments."""
        try:
            user = HabrUser.objects.get(username=username)
            message_type, _ = MessageType.objects.get_or_create(
                message_type_name='Призыв пользователя', text='Призыв пользователя', sys_type='CALL')
            text = f'Пользователь {comment.user} призывает вас в комментарии к ' \
                   f'статье "{comment.comment_article}"'

            Message.objects.create(
                user=user,
                message_type=message_type,
                text=text,
                related_article=comment.comment_article,
                related_comment=comment
            )
        except ObjectDoesNotExist:
            logger.info('User %s tried to call a non-existent '
                        'user %s in the comments', comment.user, username)


def create_article(request):
    try:
        if request.method == 'POST':
            create_form = ArticleCreationForm(request.POST)
            if create_form.is_valid():
                create_form.save()
                return HttpResponseRedirect(
                    reverse('auth:draft_user_articles', args=[request.user.habruserprofile.uuid]))
        else:
            if request.user.banned_until:
                logger.info('The banned user %s tried to write an article.', request.user.username)
                raise PermissionDenied()
            create_form = ArticleCreationForm()
        context = {'create_form': create_form}
        return render(request, 'articles/create_article.html', context)
    except PermissionDenied:
        ban_end_date = request.user.banned_until.strftime("%d.%m.%Y %H:%M")
        messages.error(request, f'Вы не можете писать статьи до {ban_end_date} по причине бана.')
        return HttpResponseRedirect(reverse('auth:profile', args=[request.user.habruserprofile.uuid]))


def update_article(request, article_slug):
    edit_article = get_object_or_404(Article, slug=article_slug)
    try:
        if request.method == "POST":
            edit_form = ArticleCreationForm(request.POST, request.FILES, instance=edit_article)
            if edit_form.is_valid():
                edit_form.save()
                return HttpResponseRedirect(
                    reverse("auth:draft_user_articles", args=[request.user.habruserprofile.uuid]))
        else:
            if request.user.banned_until:
                logger.info('The banned user %s tried to edit an article.', request.user.username)
                raise PermissionDenied()
            edit_form = ArticleCreationForm(instance=edit_article)

        content = {
            "update_form": edit_form,
            "category": edit_article.article_category,
            "media_url": settings.MEDIA_URL,
        }
        return render(request, "articles/update_article.html", content)
    except PermissionDenied:
        ban_end_date = request.user.banned_until.strftime("%d.%m.%Y %H:%M")
        messages.error(request, f'Вы не можете редактировать статьи до {ban_end_date} по причине бана.')
        return HttpResponseRedirect(reverse('auth:profile', args=[request.user.habruserprofile.uuid]))


class SearchView(ListView):
    model = Article
    template_name = 'articles/search-view.html'
    slug_url_kwarg = 'article_slug'
    context_object_name = 'context_object_name'
    paginate_by = 3

    def get_queryset(self):
        query = self.request.GET.get('search_panel')

        if query:
            self.request.session['filter'] = query

        if 'filter' in self.request.session:
            query = self.request.session['filter']

        if str(self.request).find('/new/') > -1:
            context_object_name = Article.objects.filter(Q(title__icontains=query) | Q(body__icontains=query),
                                                         is_active='ACTIVE').order_by('-create_time')
        elif str(self.request).find('/like/') > -1:
            temp_query = Article.objects.filter(Q(title__icontains=query) | Q(body__icontains=query),
                                                is_active='ACTIVE')
            context_object_name = sorted(temp_query, key=lambda t: t.get_likes(), reverse=True)
        else:
            context_object_name = Article.objects.filter(Q(title__icontains=query) | Q(body__icontains=query),
                                                         is_active='ACTIVE')

        return context_object_name

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        search_string = self.request.GET.get('search_panel')
        context['search_string'] = search_string
        try:
            user_filter = self.request.session['filter']
            if user_filter == '':
                raise KeyError
            context['title'] = f'Вы искали статью по фразе {user_filter}'
        except KeyError:
            context['title'] = 'Чтобы выполнить поиск, задайте поисковую фразу'
            self.request.session['filter'] = ''
        return context


class TagArticleView(ListView):
    """View for articles of a separate category (section)."""
    model = Article
    template_name = 'articles/read_category.html'
    paginate_by = 5
    slug_url_kwarg = 'tag_slug'
    tag = ''

    def get_queryset(self):
        """Returns a queryset of active articles of the specified category."""
        self.tag = Tag.objects.get(slug=self.kwargs.get('tag_slug'))
        return Article.objects.filter(Q(tag=self.tag), Q(is_active='ACTIVE')).select_related('article_category').only(
            'slug', 'title', 'body', 'create_time',
            'article_category__category_name')

    def get_context_data(self, *args, **kwargs):
        """Puts the title in the context according to the selected category."""
        context = super().get_context_data(**kwargs)
        context['title'] = f'Статьи с тегом {self.tag}'
        return context


class LikeToAuthorView(View):
    """View to add a like to the author of the article you like.
    The like is not added to the article, but directly to the author
    and increases his rating."""

    def post(self, request, *args, **kwargs):
        """Checks whether a particular author has received a like during the
        current session. If not, it increases the number of likes of this author,
        adds his username and the time of the like to the current session.
        If this author has already received a like within the last 5 minutes,
        it generates a message indicating the time remaining before the opportunity
        to put the next like to the same author.
        Redirects to the page of the same article."""
        article = Article.objects.get(slug=kwargs['slug'])
        author = article.user
        try:
            authors_like_time = datetime.fromisoformat(self.request.session[author.username])
            if authors_like_time + timedelta(minutes=5) < datetime.now():
                raise KeyError
            delta = ((authors_like_time + timedelta(minutes=5)) -
                     datetime.now()).total_seconds()
            messages.error(request, f'Нельзя поблагодарить одного и того же '
                                    f'автора чаще, чем раз в 5 минут. '
                                    f'Осталось минут: {int(delta // 60)} '
                                    f'секунд: {int(delta % 60)}.')
        except KeyError:
            author.likes += 1
            author.save(update_fields=['likes'])
            self.request.session[author.username] = datetime.now().isoformat()
        return HttpResponseRedirect(reverse('articles:article_read', args=[article.slug]))


class ArticleLikeRatingView(ListView):
    model = Article
    template_name = 'articles/read_category.html'
    paginate_by = 5

    def get_queryset(self):
        return Article.objects.filter(is_active='ACTIVE', like__like_comment_id=None).annotate(
            like_count=Count('like')).order_by('-like_count')[:5]

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Статьи с лучшим рейтингом'
        return context


class ArticleCommentRatingView(ListView):
    model = Article
    template_name = 'articles/read_category.html'
    paginate_by = 5

    def get_queryset(self):
        return Article.objects.filter(is_active='ACTIVE').annotate(
            comment_count=Count('comment')).order_by('-comment_count')[:5]

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Самые обсуждаемые статьи'
        return context


class UserRatingView(ListView):
    model = HabrUser
    template_name = 'articles/users_rating.html'
    paginate_by = 5

    def get_queryset(self):
        return HabrUser.objects.filter(is_active='True').order_by('-likes')[:5]

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Авторы с лучшим рейтингом'
        return context


class UserArticleView(ListView):
    model = Article
    template_name = 'articles/read_category.html'
    paginate_by = 5
    slug_url_kwarg = 'user_id'
    username = ''

    def get_queryset(self):
        self.username = HabrUser.objects.get(uuid=self.kwargs.get('user_id'))
        return Article.objects.filter(Q(user_id=self.username), Q(is_active='ACTIVE'))

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Статьи пользователя {self.username}'
        return context


class CommentDeleteView(DeleteView):
    model = Comment

    def get(self, request, *args, **kwargs):
        comment = Comment.objects.get(id=kwargs['comment_id'])
        get_object_or_404(Comment, id=self.kwargs['comment_id'])
        context = {'comment_id': self.kwargs['comment_id'], 'article': comment.comment_article, 'comment': comment}
        return render(request, 'articles/confirm_delete_comments.html', context=context)

    def post(self, request, comment_id, **kwargs):
        comment = get_object_or_404(Comment, id=comment_id)
        comment.delete()
        if not request.user == comment.user:
            message_type, _ = MessageType.objects.get_or_create(
                message_type_name='Удаление комментария', text='Комментарий удален', sys_type='COMM_DEL')
            text = f'Ваш комментарий "{comment.body}" удален модератором {request.user}.' \
                   f'\nПожалуйста, придерживайтесь правил корректного общения и ' \
                   f'комментирования, принятых на нашей платформе.'
            self.message_create(comment.user, text, message_type)
            if 'is_banned' in request.POST:
                self.auto_ban_user(request, comment)
        return HttpResponseRedirect(reverse('articles:article_read',
                                            args=[comment.comment_article.slug]))

    def message_create(self, user, text, message_type):
        """Creating a new message for the user."""
        Message.objects.create(
            user=user,
            message_type=message_type,
            text=text
        )

    def auto_ban_user(self, request, comment):
        """Automatic ban of the user in case of serious violations detected during
        the moderation of his comment."""
        user = comment.user
        user.banned_until = COMMENTS_BAN
        user.save()
        message_type, _ = MessageType.objects.get_or_create(
            message_type_name='Бан пользователя', text='Пользователь забанен', sys_type='BAN')
        text = f'Вы забанены модератором {request.user}.' \
               f'\nПричина: Серьезные нарушения правил общения и комментирования' \
               f' в оставленном вами комментарии "{comment.body}".'
        self.message_create(user, text, message_type)


class CommentLikeView(DetailView):
    model = Comment

    def post(self, request, comment_id, **kwargs):
        comment = Comment.objects.get(id=comment_id)
        likes = Like.objects.all().filter(user=self.request.user.uuid,
                                          like_comment=comment_id).count()
        self.object = get_object_or_404(Comment, id=comment_id)
        if likes == 0:
            Like.objects.create(user=self.request.user, like_comment=self.object)
        else:
            like = Like.objects.get(user=self.request.user.uuid, like_comment=comment_id)
            like.delete()
        return HttpResponseRedirect(reverse('articles:article_read',
                                            args=[comment.comment_article.slug]))
