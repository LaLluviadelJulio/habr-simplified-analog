"""Contains custom template tags."""

from django import template

from authnapp.models import HabrUserProfile
from ..models import Category
register = template.Library()


@register.inclusion_tag('articles/includes/menu.html', takes_context=True)
def show_categories(context):
    """Template tag for displaying menu sections (categories of articles from the database)."""
    menu_context = context
    session = context['request'].session
    try:
        menu_context['category_menu_items'] = session['category_menu_items']
    except KeyError:
        menu_context['category_menu_items'] = \
            session['category_menu_items'] = Category.objects.filter(is_active=True)
    if not context['request'].user.is_anonymous:
        try:
            menu_context['profile'] = session['profile']
        except KeyError:
            menu_context['profile'] = \
                session['profile'] = HabrUserProfile.objects.get(user=context['request'].user)
    return menu_context
