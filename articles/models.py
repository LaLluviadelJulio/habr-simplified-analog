"""
Stores article, category and tag models.
"""
import logging
import shutil
from datetime import datetime
from logging import Logger
from urllib import request

from PIL import Image, ExifTags
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.db import models, IntegrityError, transaction
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.utils.safestring import mark_safe
from django.core.mail import send_mail
from pytils.translit import slugify
from django.conf import settings
from django_currentuser.middleware import get_current_user, get_current_authenticated_user
from django_currentuser.db.models import CurrentUserField
from mptt.models import MPTTModel, TreeForeignKey

from authnapp.models import HabrUser

logger: Logger = logging.getLogger(__name__)

IMAGE_MAX_SIZE = 600


def user_image_path(instance, filename):
    """Generates and returns the path for the saved images of the user.
        It is necessary for the orderly storage of images.

        Args:

            * instance (`PictureFile`): an instance of a picture being created or modified.
            * filename (`str`): the name under which the image will be stored.

        Returns:

            * str: the path to the saved file.
        """
    return f'media/{instance.user}/{filename}'


class Base(models.Model):
    """The model for the article."""
    create_time = models.DateTimeField(verbose_name="время создания", default=datetime.now)
    update_time = models.DateTimeField(verbose_name="время изменения", default=datetime.now)
    slug = models.SlugField(max_length=255, unique=True, db_index=True, verbose_name="URL")

    def save(self, *args, slugified_field=None, **kwargs):
        """Automatic filling in update_time and the slug fields when saving."""
        self.update_time = datetime.now()
        if slugified_field:
            try:
                with transaction.atomic():
                    self.slug = old_slug = slugify(slugified_field)
                    super().save(*args, **kwargs)
            except IntegrityError:
                with transaction.atomic():
                    self.slug = slugify(slugified_field + str(datetime.now()))
                    logger.error(f'Non-unique slug {old_slug} replaced with {self.slug}')
                    super().save(*args, **kwargs)
        else:
            super().save(*args, **kwargs)


    class Meta:
        abstract = True


class BaseActive(Base):
    """The base model for the activity."""
    is_active = models.BooleanField(verbose_name="активно", default=True, db_index=True)


    class Meta:
        abstract = True


class ArticleActive(Base):
    """The article model for the activity."""
    class IsActive(models.TextChoices):
        DRAFT = 'DRAFT'
        DENY = 'DENY'
        ACTIVE = 'ACTIVE'
        MODER = 'MODER'
        USRDEL = 'USRDEL'

    is_active = models.CharField(
        max_length=6,
        choices=IsActive.choices,
        default=IsActive.DRAFT,
        verbose_name="статус"
    )


    class Meta:
        abstract = True


class Category(BaseActive):
    """The model for the category."""
    category_name = models.CharField(max_length=128)

    def __str__(self):
        """Forms a printable representation of the object.
        Returns the name of the category.
        """
        return str(self.category_name)

    def save(self, *args, **kwargs):
        """Automatic filling in update_time and the slug field when saving."""
        super().save(*args, slugified_field=self.category_name, **kwargs)

    class Meta:
        """Ordering categories according to their id.
        """
        ordering = ('id',)
        verbose_name = 'Раздел сайта'
        verbose_name_plural = 'Разделы сайта'


class Tag(BaseActive):
    """The model for the tag."""
    tag_name = models.CharField(verbose_name="наименование", max_length=128, blank=False)

    def __str__(self):
        """Forms a printable representation of the object.
        Returns the name of the tag.
        """
        return str(self.tag_name)

    def save(self, *args, **kwargs):
        """Automatic filling in update_time and the slug fields when saving."""
        super().save(*args, slugified_field=self.tag_name, **kwargs)

    def get_articles_count(self):
        """Counting likes to a comment."""
        articles = Article.objects.all().filter(tag=self).count()
        return articles

    class Meta:
        """Ordering users according to their id.
        """
        ordering = ('tag_name',)
        verbose_name = 'Теги'
        verbose_name_plural = 'Тег'


class Article(ArticleActive):
    """The model for the article."""
    title = models.CharField(verbose_name="заголовок", max_length=128, blank=False, validators=[
        MinLengthValidator(10), MaxLengthValidator(128)])
    body = models.TextField(verbose_name="тело", blank=False, validators=[
        MinLengthValidator(100), MaxLengthValidator(100000)])
    tag = models.ManyToManyField(Tag)
    user = CurrentUserField(HabrUser, on_delete=models.CASCADE,
                             null=False, blank=False)
    article_category = models.ForeignKey(Category, on_delete=models.CASCADE,
                                         null=False, blank=False)
    rejection_reason = models.CharField(verbose_name="причины отклонения статьи", max_length=250, blank=True)

    def __str__(self):
        """Forms a printable representation of the object.
        Returns the title of the article.
        """
        return str(self.title)

    def save(self, *args, **kwargs):
        """Automatic filling in update_time and the slug fields when saving."""
        super().save(*args, slugified_field=self.title, **kwargs)

    def get_likes(self):
        """Counting likes to an article."""
        likes = Like.objects.all().filter(like_article=self, like_comment_id=None).count()
        return likes

    def get_comments(self):
        """Counting comments to an article."""
        comments = Comment.objects.all().filter(comment_article=self).count()
        return comments

    class Meta:
        """Ordering articles according to their date of addition (the most recent ones first).
        """
        ordering = ['-create_time', ]
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'


class Comment(BaseActive, MPTTModel):
    """The model for the comment."""
    body = models.CharField(verbose_name="тело", max_length=100000, blank=False, validators=[
        MinLengthValidator(5), MaxLengthValidator(100000)])
    user = CurrentUserField(HabrUser, on_delete=models.CASCADE, null=False, blank=False)
    comment_article = models.ForeignKey(Article, on_delete=models.CASCADE, null=False, blank=False, verbose_name='Родительская статья')
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children', db_index=True, verbose_name='Родительский комментарий')

    def __str__(self):
        """Forms a printable representation of the object.
        Returns the title of the comment.
        """
        return str(self.body)

    def save(self, *args, **kwargs):
        """Automatic filling in update_time and the slug field when saving."""
        super().save(*args, slugified_field=self.body[:30], **kwargs)

    def get_likes(self):
        """Counting likes to a comment."""
        likes = Like.objects.all().filter(like_comment=self).count()
        return likes

    class MPTTMeta:
        order_insertion_by = ['create_time',]

    class Meta:
        """Ordering comments according to their date of addition (the most recent ones first).
        """
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'


class Like(BaseActive):
    """The model for the likes."""
    user = models.ForeignKey(HabrUser, on_delete=models.CASCADE, null=False)
    like_article = models.ForeignKey(Article, on_delete=models.CASCADE, null=True, blank=True)
    like_comment = models.ForeignKey(Comment, on_delete=models.CASCADE, null=True, blank=True)
    like_user = models.ForeignKey(HabrUser, on_delete=models.CASCADE, null=False, blank=False,
                                  related_name='like_user')

    def __str__(self):
        """Forms a printable representation of the object.
        Returns the title of the like.
        """
        return str(self.user.username)

    def save(self, *args, **kwargs):
        """Automatic filling in update_time and the slug fields when saving.
        Automatic filling in like_user field"""
        slug_autofill = str(self.user) + str(datetime.now())
        if self.like_article:
            self.like_user = self.like_article.user
        elif self.like_comment:
            self.like_user = self.like_comment.user
        super().save(*args, slugified_field=slug_autofill, **kwargs)

    class Meta:
        """Ordering likes according to their date of addition (the most recent ones first).
        """
        ordering = ['-create_time', ]
        verbose_name = 'Лайк'
        verbose_name_plural = 'Лайки'
        constraints = [
            models.UniqueConstraint(fields=['user', 'like_article'], name='unique_user_like_article'),
            models.UniqueConstraint(fields=['user', 'like_comment'], name='unique_user_like_comment'),
        ]


class MessageType(BaseActive):
    """The model for the message type."""

    class SysType(models.TextChoices):
        HELLO = 'HELLO'
        BAN = 'BAN'
        DENY = 'DENY'
        APPROVE = 'APPROVE'
        RECOVER = 'RECOVER'
        CALL = 'CALL'
        COMM_DEL = 'COMM_DEL'

    message_type_name = models.CharField(verbose_name="тип модели", max_length=128, blank=False)
    text = models.CharField(verbose_name="текст шаблона", max_length=100000, blank=True)
    sys_type = models.CharField(
        max_length=10,
        choices=SysType.choices,
        default=SysType.BAN,
        verbose_name="вид сообщения"
    )

    def __str__(self):
        """Forms a printable representation of the object.
        Returns the title of the message type.
        """
        return str(self.message_type_name)

    def save(self, *args, **kwargs):
        """Automatic filling in update_time and the slug fields when saving."""
        super().save(*args, slugified_field=self.message_type_name, **kwargs)

    class Meta:
        verbose_name = 'Тип сообщения'
        verbose_name_plural = 'Тип сообщений'


class Message(BaseActive):
    """The model for the message."""
    user = models.ForeignKey(HabrUser, on_delete=models.CASCADE, null=False)
    message_type = models.ForeignKey(MessageType, on_delete=models.CASCADE, null=False, blank=False)
    text = models.CharField(verbose_name="текст", max_length=100000, blank=True)
    related_article = models.ForeignKey(Article, on_delete=models.CASCADE, null=True, blank=True)
    related_comment = models.ForeignKey(Comment, on_delete=models.CASCADE, null=True, blank=True)
    is_read = models.BooleanField(verbose_name="прочитано", default=False)

    def __str__(self):
        """Forms a printable representation of the object.
        Returns the title of the message.
        """
        return str(self.text)[:100]

    def save(self, *args, **kwargs):
        """Automatic filling in update_time and the slug fields when saving."""
        slug_autofill = str(self.user) + str(self.text[:30])
        super().save(*args, slugified_field=slug_autofill, **kwargs)

        title = f"Новое сообщение с Habr ({self.message_type})"
        message = self.text
        try:
            return send_mail(
                title,
                message,
                settings.EMAIL_HOST_USER,
                [self.user.email],
                fail_silently=False,
            )
        except Exception:
            pass

    def get_cropped_text(self):
        """Trims the existing large text for a more convenient display,
        for example, in the admin panel."""
        return self.text[:20]

    class Meta:
        """Ordering messages according to their date of addition (the most recent ones first).
        """
        ordering = ['-create_time', ]
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'


class PictureFile(BaseActive):
    """The model for the picture file."""
    file_name = models.CharField(verbose_name="имя файла", max_length=128, blank=False)
    user = models.ForeignKey(HabrUser, on_delete=models.CASCADE, null=False)
    picture_file = models.ImageField(upload_to=user_image_path)

    def __str__(self):
        """Forms a printable representation of the object.
        Returns the title of the message type.
        """
        return str(self.file_name)

    def save(self, *args, **kwargs):
        """Saves a new object, if there is an image file, if necessary,
        reduces it to an acceptable size and expands it for correct display."""
        super().save(*args, slugified_field=self.file_name[:30], **kwargs)
        if self.picture_file:
            try:
                width, height = self.picture_file.width, self.picture_file.height
                max_size = max(width, height)
                img = Image.open(self.picture_file.path)
                if max_size > IMAGE_MAX_SIZE:
                    img = img.resize(
                        (round(width / max_size * IMAGE_MAX_SIZE),
                         round(height / max_size * IMAGE_MAX_SIZE)),
                        Image.ANTIALIAS
                    )
                for orientation in ExifTags.TAGS.keys():
                    if ExifTags.TAGS[orientation] == 'Orientation':
                        break
                exif = dict(img.getexif().items())
                image = img.rotate({3: 180, 6: 270, 8: 90}.get(exif[orientation], 0), expand=True)
                image.save(self.picture_file.path)
            except (AttributeError, KeyError, IndexError) as error:
                logging.error(error, exc_info=True)

    def delete(self, using=None, keep_parents=False):
        """When an object is deleted, it deletes the corresponding image file."""
        if self.picture_file:
            self.picture_file.delete(save=False)
        super().delete()

    @property
    def image_url(self):
        """If there is an image for this object and a path to it, it returns this path."""
        if self.picture_file and hasattr(self.picture_file, 'url'):
            return self.picture_file.url

    def image_tag(self):
        """Returns a html element that has the path to the image file embedded in it
         in order to display it correctly in the admin panel."""
        if self.picture_file:
            return mark_safe(f'<img src="{self.image_url}" width="20%" height="20%" />')
        logger.error("Unable to find the path to the picture: %s", self.image_url)
        return mark_safe('<img src="/static/vendor/img/default.png" style="width: '
                         '20%; height:20%;" />')

    image_tag.short_description = 'Миниатюра картинки'

    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки'
        ordering = ['-update_time', ]


@receiver(pre_delete, sender=HabrUser)
def delete_users_images_folder(sender, instance, **kwargs):
    """The signal initiates the deletion of the folder with
    the user's pictures if this user is deleted."""
    try:
        shutil.rmtree(f'static/images/{instance}')
    except FileNotFoundError as error:
        logging.error(error, exc_info=True)
