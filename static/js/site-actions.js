/**
 * Contains javascript scripts for asynchronous operation of site elements.
 */

window.addEventListener('load', (e) => {
    /**
     * Works with any pictures except user's avatar. When you click on the image,
     * it will be opened in a modular window and enlarged for the convenience of the user.
     */
    $('div.thumbnail').click(function (e) {
        e.preventDefault();
        let base_dir = '![](http://127.0.0.1:8000'
        $('#image-modal .modal-body img').attr('src', $(this).find('img').attr('src')).attr('id', $(this).attr('id'));
        $('input.image-url').attr('value', base_dir + $(this).find('img').attr('src').trimStart() + ')');
        $("#image-modal").modal('show');
    });

    /**
     * Closes the modal window of the image when you click on it.
     * This modal window is used to zoom in the picture,
     * so that it is more convenient for the user to view the details.
     */
    $('#image-modal .modal-body img').on('click', function () {
        $("#image-modal").modal('hide')
    });

    /**
     * Tracks the current page and if it relates to working with images,
     * then the search bar changes the placeholder and the action when
     * using it to search for images.
     */
    if (location.pathname.search(/images/i) !== -1) {
        let pathname = window.location.pathname.split("/")
        let uuid = pathname[pathname.length - 2]
        let search_images_path = `/auth/search_images/${uuid}`
        $('#search_panel').attr('placeholder', 'Поиск по картинкам')
        $('#form_search_panel').attr('action', search_images_path)
    }
});
