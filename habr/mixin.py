"""Contains mixins for convenient addition of views logic."""
from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator
from django.views.generic.base import View, ContextMixin


class AuthorizedOnlyDispatchMixin(View):
    """A mixin class for providing access to the corresponding page only to authorized users."""
    @method_decorator(user_passes_test(lambda u: u.is_authenticated))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class ModeratorOnlyDispatchMixin(View):
    """A mixin class for providing access to the corresponding page only to moderators."""
    @method_decorator(user_passes_test(lambda u: u.is_staff))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class TitleMixin(ContextMixin):
    """A mixin class for adding page titles."""
    title = ''

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.title
        return context
