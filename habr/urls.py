"""habr URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny

from articles.views import MainView, SearchView, page_not_found_view, create_article, help_view, update_article, \
    ArticleCommentRatingView, ArticleLikeRatingView, UserRatingView

schema_view = get_schema_view(
    openapi.Info(
        title="Habr",
        default_version='0.1',
        description="Documentation to out project",
        contact=openapi.Contact(email="admin@admin.local"),
        license=openapi.License(name="MIT License"),
    ),
    public=True,
    permission_classes=[AllowAny],
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", MainView.as_view(), name="index"),
    path("articles/", include("articles.urls", namespace="articles")),
    path("auth/", include("authnapp.urls", namespace="auth")),
    path('moderation/', include('moderation.urls', namespace='moderation')),
    path("", include("social_django.urls", namespace="social")),
    path('create_article/', create_article, name='create_article'),
    path('update_article/<slug:article_slug>/', update_article, name='update_article'),
    path('search/', SearchView.as_view(), name='search_view'),
    path('search/like/<str:search_string>/', SearchView.as_view(), name='search_view_sort_like'),
    path('search/new/<str:search_string>/', SearchView.as_view(), name='search_view_sort_new'),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('help/', help_view, name='help'),
    path('404/', page_not_found_view,
         kwargs={'exception': Exception('Page not Found')}, name='page_404'),
    path('martor/', include('martor.urls')),
    path("best_articles/", ArticleLikeRatingView.as_view(), name="best_articles"),
    path("discussed_articles/", ArticleCommentRatingView.as_view(), name="discussed_articles"),
    path("users_rating/", UserRatingView.as_view(), name="users_rating"),
]

handler404 = "habr.views.page_not_found_view"

urlpatterns += [re_path('^__debug_/', include(debug_toolbar.urls))]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [path('404/', page_not_found_view,
                         kwargs={'exception': Exception('Page not Found')}, name='page_404')]
