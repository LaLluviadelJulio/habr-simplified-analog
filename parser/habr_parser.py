import os
from pathlib import Path

import wget
from bs4 import BeautifulSoup
from markdownify import markdownify


def html_to_md_converter(url):
    file_name = f"{url.split('/')[-2]}"  # Получаем из url название файла
    try:
        os.mkdir("media")  # Создаём папку media
    except Exception as ex:
        pass
    try:
        os.mkdir("html")  # Создаём папку html
    except Exception as ex:
        pass
    os.mkdir(Path("media", file_name))  # Создаём папку c названием URL статьи
    html_file = f"{file_name}.html"  # Имя HTML-файла
    md_file = f"{file_name}.md"  # Имя MD-файла
    path_html_file = str(Path("html", html_file))  # Путь до HTML-файла

    # Скачиваем страницу
    wget.download(url, path_html_file)
    # Читаем скачанный файл
    with open(path_html_file, "r") as f:
        src = f.read()
    # Подключаем парсер
    soup = BeautifulSoup(src, "lxml")
    # Получаем название страницы
    title = soup.title
    print("\n", str(title.text)[:-7])
    # Сохраняем в файл название страницы
    with open(str(Path("media", file_name, "title.txt")), "w") as f:
        f.write(str(title.text)[:-7])
    # Находим кусок HTML-кода с текстом статьи
    html_text = soup.find("div", class_="tm-article-body")
    # Конвертируем в MD
    res = markdownify(str(html_text), heading_style="ATX")
    # Добавляем переносы строк после картинок
    res = res.replace("webp)", "webp)\n")
    res = res.replace("jpeg)", "jpeg)\n")
    res = res.replace("png)", "png)\n")
    # Записываем MD-текст в файл
    with open(str(Path("media", file_name, md_file)), "w") as f:
        f.write(res)
    # Читаем файл построчно и приводим ссылки на картинки к нужному формату
    with open(str(Path("media", file_name, md_file)), "r") as f:
        lines = f.readlines()
        for line in lines:
            if line.find("](https://habrastorage") != -1:
                line = line.split("](")[1]
                url = line.split()[0]
                url = url.split(")")[0]
                # print(url)
                # Скачиваем картинки в папку media
                try:
                    wget.download(url, out=f'{Path("media", file_name)}')
                except Exception as ex:
                    print(ex)


design_url_list = [
    "https://habr.com/ru/post/677498/",
    "https://habr.com/ru/post/677360/",
    "https://habr.com/ru/post/677272/",
    "https://habr.com/ru/post/677172/",
    "https://habr.com/ru/post/676436/",
    "https://habr.com/ru/post/676806/",
    "https://habr.com/ru/post/676352/",
    "https://habr.com/ru/post/675736/",
    "https://habr.com/ru/post/675658/",
    "https://habr.com/ru/post/676480/",
]

marketing_url_list = [
    "https://habr.com/ru/post/677486/",
    "https://habr.com/ru/company/oleg-bunin/blog/677474/",
    "https://habr.com/ru/post/677352/",
    "https://habr.com/ru/post/677210/",
    "https://habr.com/ru/company/mygames/blog/677074/",
    "https://habr.com/ru/company/pt/blog/677114/",
    "https://habr.com/ru/company/timeweb/blog/676072/",
    "https://habr.com/ru/post/676626/",
    "https://habr.com/ru/company/globalsign/blog/676388/",
    "https://habr.com/ru/company/visiology/blog/676300/",
]

mobile_dev_url_list = [
    "https://habr.com/ru/post/677610/",
    "https://habr.com/ru/post/677598/",
    "https://habr.com/ru/company/jugru/blog/677326/",
    "https://habr.com/ru/company/kaspersky/blog/677026/",
    "https://habr.com/ru/company/etmc_exponenta/blog/677524/",
    "https://habr.com/ru/company/ruvds/blog/677454/",
    "https://habr.com/ru/post/677512/",
    "https://habr.com/ru/company/akbarsdigital/blog/677488/",
    "https://habr.com/ru/company/selectel/blog/676868/",
    "https://habr.com/ru/company/dsec/blog/677204/",
]

web_dev_url_list = [
    "https://habr.com/ru/post/677476/",
    "https://habr.com/ru/company/bizone/blog/677180/",
    "https://habr.com/ru/company/ncloudtech/blog/676960/",
    "https://habr.com/ru/company/rshb/blog/677472/",
    "https://habr.com/ru/company/wunderfund/blog/677068/",
    "https://habr.com/ru/post/677456/",
    "https://habr.com/ru/company/ruvds/blog/676780/",
    "https://habr.com/ru/post/677444/",
    "https://habr.com/ru/post/677442/",
    "https://habr.com/ru/company/pvs-studio/blog/677440/",
]

for url in web_dev_url_list:
    html_to_md_converter(url)
