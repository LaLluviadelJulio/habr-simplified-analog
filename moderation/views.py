"""The submodule contains a views for the moderator's full-fledged work: approving and rejecting
articles and comments, assigning and removing a ban for users.

Here are views:

    * to display the main page of the moderation panel;
    * to display all registered users of the site;
    * for individual user moderation;
    * to delete (deactivate)/activate user;
    * to assign/remove a ban to a user;
    * to view all articles submitted for pre-moderation;
    * to view and make a decision on a separate article;
    * to view the list of rejected articles;
"""

import logging
import re
from datetime import datetime, timedelta

from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic import TemplateView, ListView, DeleteView, UpdateView, DetailView
from django.core.exceptions import ObjectDoesNotExist
from articles.models import Article, Message, MessageType
from authnapp.models import HabrUser
from habr.mixin import TitleMixin, ModeratorOnlyDispatchMixin
from moderation.forms import UserBanForm

logger = logging.getLogger(__name__)

OFFENSES_BAN = datetime.now() + timedelta(days=14)

def message_create(user, text, message_type):
    """Creating a new message for the user."""
    Message.objects.create(
        user=user,
        message_type=message_type,
        text=text
    )


class ModerationPanelView(TemplateView, TitleMixin, ModeratorOnlyDispatchMixin):
    """View for the main moderation panel page."""
    template_name = 'moderation/moderation-index.html'
    title = 'Панель модератора'

    def get_context_data(self, **kwargs):
        """Returns a context with the counted number of articles awaiting
        moderation and active moderator's calls."""
        context = super().get_context_data()
        context['articles_number'] = Article.objects.filter(~Q(user=self.request.user),
                                                            Q(user__banned_until=None),
                                                            Q(is_active='MODER')).count()
        context['active_calls'] = Message.objects.filter(Q(user__username='moderator'),
                                                         Q(is_read=False)).count()
        return context


class UserListView(ListView, TitleMixin, ModeratorOnlyDispatchMixin):
    """View to view lists of all registered users of the site in the moderation panel."""
    model = HabrUser
    template_name = 'moderation/users/users-viewing.html'
    context_object_name = 'users'
    title = 'Просмотр пользователей'
    paginate_by = 3


class UserDetailView(DetailView, TitleMixin, ModeratorOnlyDispatchMixin):
    """View to update a specific user in the moderation panel."""
    model = HabrUser
    template_name = 'moderation/users/users-detail.html'
    title = 'Модерация пользователя'


class UserDeleteView(DeleteView, ModeratorOnlyDispatchMixin):
    """View to delete or activate/deactivate a specific user in the moderation panel."""
    model = HabrUser
    template_name = 'moderation/users/users-detail.html'
    success_url = reverse_lazy('moderation:moderation_users')

    def delete(self, request, *args, **kwargs):
        """Activation/deactivation of the user.
        A moderator cannot deactivate himself, other moderators and administrators."""
        user = self.get_object()
        try:
            if request.user.uuid != kwargs['pk']:
                if user.is_superuser or user.is_staff:
                    logger.error('Attempt to delete another team member'
                                 ' %s by the moderator %s.', user, request.user.username)
                    raise PermissionDenied()
                user.is_active = not bool(user.is_active) if user.is_active is True else True
                # здесь можно сделать вызов метода создания и отправки
                # сообщения об удалении/восстановлении
                user.save()
                return redirect(self.success_url)
            logger.error('Attempt to delete the moderator %s himself.', request.user.username)
            raise PermissionDenied()
        except PermissionDenied:
            messages.error(request, 'Вы не можете удалить сами себя или другого сотрудника сайта.')
            return render(request, self.template_name,
                          context={'habruser': user})


class UserGetsBannedView(UpdateView, TitleMixin, ModeratorOnlyDispatchMixin):
    """View for the ban or unban of a specific user in the moderator panel."""
    model = HabrUser
    template_name = 'moderation/users/user-ban.html'
    form_class = UserBanForm
    success_url = reverse_lazy('moderation:moderation_users')
    title = 'Бан пользователя'

    def get(self, request, *args, **kwargs):
        """Checks whether the object of the ban is an employee of the site,
        and also whether the moderator is trying to ban himself."""
        user = self.get_object()
        try:
            if request.user != user:
                if user.is_superuser or user.is_staff:
                    logger.error('Attempt to ban another team member'
                                 ' %s by the moderator %s.', user, request.user.username)
                    raise PermissionDenied()
                return render(request, self.template_name,
                              context={'form': self.form_class, 'habruser': user, 'title': self.title})
            logger.error('Attempt to ban the moderator %s himself.', request.user.username)
            raise PermissionDenied()
        except PermissionDenied:
            messages.error(request, 'Вы не можете забанить сами себя или другого сотрудника сайта.')
            return render(request, 'moderation/users/users-detail.html',
                          context={'habruser': user, 'title': self.title})

    def post(self, request, *args, **kwargs):
        """The user is banned/unban for the period entered by the moderator in the form.
        If the date or time value is incorrect, it returns a form with the entered data
        and a message about their incorrectness."""
        user = self.get_object()
        if user.banned_until:
            user.banned_until = None
            user.save()
            try:
                message_sys_type = MessageType.objects.get(message_type_name='Бан пользователя')
                Message.objects.create(message_type=message_sys_type,
                                       user=user,
                                       text=f'Дорогой наш {user.first_name} {user.last_name}, '
                                            f'ты снова теперь хороший и разбанен!')
            except ObjectDoesNotExist:
                pass
            return redirect(self.success_url)
        try:
            user.banned_until = datetime.strptime\
                (request.POST['banned_until'], "%d/%m/%Y %H:%M")
            if user.banned_until < datetime.now():
                user.banned_until = None
                raise ValueError
            try:
                message_sys_type = MessageType.objects.get(message_type_name='Бан пользователя')
                Message.objects.create(message_type=message_sys_type,
                                       user=user,
                                       text=f'Дорогой наш {user.first_name} {user.last_name}, '
                                            f'ты редиска и забанен!')
            except ObjectDoesNotExist:
                pass
            user.save()
        except ValueError:
            logger.error('Invalid date or time value when attempting to ban a user.')
            messages.error(request, 'Некорректное значение даты или времени '
                                    'при попытке бана пользователя.')
            return render(request, self.template_name,
                          context={'form': self.form_class(request.POST),
                                   'habruser': user})
        return redirect(self.success_url)


class ArticlesModerListView(ListView, TitleMixin, ModeratorOnlyDispatchMixin):
    """View to view the list of articles on moderation in the moderation panel."""
    model = Article
    template_name = 'moderation/articles/articles-viewing.html'
    context_object_name = 'moder_articles'
    title = 'Просмотр статей на модерации'
    paginate_by = 3

    def get_queryset(self):
        """Returns a queryset of articles with the status "MODER",
        the authorship of which does not belong to the current moderator."""
        return Article.objects.filter(~Q(user=self.request.user),
                                      Q(user__banned_until=None),
                                      Q(is_active='MODER')). \
            order_by('article_category')


class ArticleModerDetailView(DetailView, TitleMixin, ModeratorOnlyDispatchMixin):
    """View to make a decision on the publication/rejection of an article
    that is being moderated in the moderation panel."""
    model = Article
    template_name = 'moderation/articles/article-detail.html'
    slug_url_kwarg = 'article_slug'
    title = 'Модерация статьи'

    def post(self, request, *args, **kwargs):
        """Receives information about the result of article moderation,
        generates a message for the user about it (publication or rejection
        of the article).
        Automatically adds the reasons for rejection noted by the moderator
        to inform the author of the article. In the presence of serious violations,
        it automatically bans the user and generates a message about it to him."""
        article = self.get_object()
        try:
            reasons = request.POST['mydata']
            if reasons == 'ok':
                article.is_active = 'ACTIVE'
                article.rejection_reason = ''
                message_type, _ = MessageType.objects.get_or_create(
                    message_type_name='Публикация статьи', text='Статья опубликована',
                    sys_type='APPROVE')
                text = f'Ваша статья {article.title} опубликована, благодарим за полезный вклад!'
            else:
                article.is_active = 'DENY'
                reasons = re.sub(r'\s+', ' ', reasons)
                article.rejection_reason = reasons
                message_type, _ = MessageType.objects.get_or_create(
                    message_type_name='Отклонение статьи', text='Статья отклонена', sys_type='DENY')
                text = f'Ваша статья {article.title} отклонена модератором {request.user}.' \
                       f'\nПричины: {reasons}.'
            article.save()
            message_create(article.user, text, message_type)
            if 'ban_reason' in request.POST:
                self.auto_ban_user(request, article)
            return redirect(reverse_lazy('moderation:moderation_articles'))
        except MultiValueDictKeyError:
            logger.error('An error in the received data when processing the reasons '
                         'for rejection or publication of the article.')
            messages.error(request, 'Произошла ошибка в процессе модерации статьи. '
                                    'Попробуйте выполнить действие еще раз.')
            return render(request, self.template_name,
                          context={'title': self.title, 'article': article})

    def auto_ban_user(self, request, article):
        """Automatic ban of the user in case of serious violations detected during
        the moderation of his article."""
        user = article.user
        user.banned_until = OFFENSES_BAN
        user.save()
        message_type, _ = MessageType.objects.get_or_create(
            message_type_name='Бан пользователя', text='Пользователь забанен', sys_type='BAN')
        text = f'Вы забанены модератором {request.user}.' \
               f'\nПричина: Нарушение положений одной из статей УК РФ: ' \
               f'148, 280, 282, 205.2 при написании статьи {article.title}.'
        message_create(user, text, message_type)


class ArticleRejectedListView(ListView, TitleMixin, ModeratorOnlyDispatchMixin):
    """View to view the list of rejected articles in the moderation panel."""
    model = Article
    template_name = 'moderation/articles/articles-viewing.html'
    context_object_name = 'deny_articles'
    title = 'Просмотр отклоненных статей'
    paginate_by = 3

    def get_queryset(self):
        """Returns a queryset of articles with the status "DENY",
        the authorship of which does not belong to the current moderator."""
        return Article.objects.filter(~Q(user=self.request.user),
                                      Q(is_active='DENY')). \
            order_by('article_category')


class ArticleRejectedDetailView(DetailView, TitleMixin, ModeratorOnlyDispatchMixin):
    """A view to restore an article from rejected ones, if for some reason
    it was decided to do so."""
    model = Article
    template_name = 'moderation/articles/article-detail.html'
    slug_url_kwarg = 'article_slug'
    title = 'Восстановление статьи из отклоненных'

    def post(self, request, *args, **kwargs):
        """Assigns the article the status "under moderation",
        generates a message to the user about it."""
        article = self.get_object()
        article.is_active = 'MODER'
        article.rejection_reason = ''
        message_type, _ = MessageType.objects.get_or_create(
            message_type_name='Восстановление статьи', text='Статья восстановлена из отклоненных',
            sys_type='RECOVER')
        text = f'Ваша статья {article.title} восстановлена из отклоненных, ' \
               f'приносим извинения за произошедшую ошибку.'
        article.save()
        user = article.user
        message_create(user, text, message_type)

        if user.banned_until:
            self.auto_remove_ban_user(request, article)

        return redirect(reverse_lazy('moderation:moderation_articles'))

    def auto_remove_ban_user(self, request, article):
        """If the user was banned for this article
        and after that there were no other reasons for the ban,
        then he will be restored from the ban and will receive a message about it."""
        user = article.user
        last_ban_message = Message.objects.filter(user=user).order_by('-update_time')[0]
        if article.title in last_ban_message.text:
            user.banned_until = None
            user.save()
            message_type, _ = MessageType.objects.get_or_create(
                message_type_name='Восстановление из бана пользователя',
                text='Пользователь восстановлен из бана', sys_type='RECOVER')
            text = f'Вы восстановлены из бана модератором {request.user}.' \
                   f'\nПриносим извинения за ошибку, если вы попали в бан по данной причине.'
            message_create(user, text, message_type)


class MessagesListView(ListView, TitleMixin, ModeratorOnlyDispatchMixin):
    """A view to view the moderator's calls in the moderation panel."""
    model = Message
    template_name = 'moderation/messages/messages-viewing.html'
    context_object_name = 'messages'
    title = 'Призывы пользователей'
    paginate_by = 3

    def get_queryset(self):
        """Returns all user messages for the addressee `moderator`."""
        return Message.objects.filter(user__username='moderator')
