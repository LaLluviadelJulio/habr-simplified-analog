from django.urls import path

from moderation.views import ModerationPanelView, UserListView,\
    UserDetailView, UserDeleteView, UserGetsBannedView, \
    ArticlesModerListView, ArticleModerDetailView, ArticleRejectedListView, \
    ArticleRejectedDetailView, MessagesListView

app_name = 'moderation'


urlpatterns = [
    path('', ModerationPanelView.as_view(), name='moderation_index'),

    path('users/', UserListView.as_view(), name='moderation_users'),
    path('user-moderation/<uuid:pk>/', UserDetailView.as_view(), name='moderation_user_detail'),
    path('user-delete/<uuid:pk>/', UserDeleteView.as_view(), name='moderation_user_delete'),
    path('user-ban/<uuid:pk>/', UserGetsBannedView.as_view(), name='moderation_user_ban'),

    path('articles/', ArticlesModerListView.as_view(), name='moderation_articles'),
    path('article-moderation/<slug:article_slug>/', ArticleModerDetailView.as_view(),
         name='moderation_article_detail'),
    path('deny-articles/', ArticleRejectedListView.as_view(), name='deny_articles'),
    path('article-recover/<slug:article_slug>/', ArticleRejectedDetailView.as_view(),
         name='recover_article_detail'),

    path('calls/', MessagesListView.as_view(), name='moderation_calls'),
]
