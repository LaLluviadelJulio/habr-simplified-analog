from django import forms
from django.contrib.auth.forms import UserChangeForm
from authnapp.models import HabrUser


class UserBanForm(UserChangeForm):
    """Form for banning users."""
    banned_until = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'])

    class Meta:
        """Defines the model involved and the fields used in the form.
        """
        model = HabrUser
        fields = ('username', 'banned_until')

    def __init__(self, *args, **kwargs):
        """Manages the class of form field 'username' for its correct display on the page.
        Sets the read-only mode for the 'username' field to prohibit its editing.
        """
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['readonly'] = True
        self.fields['username'].widget.attrs['class'] = 'form-control py-4'
